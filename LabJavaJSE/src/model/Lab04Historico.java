package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;

public class Lab04Historico {

	private int numAge;
	private int numConta;

	private int dia, mes, ano;

	private int hora, min, seg;

	private int codHist;

	private double valor;

	Vector<String> vetOperacoes;

	public Lab04Historico(int numAge, int numConta) {
		super();
		this.numAge = numAge;
		this.numConta = numConta;
	}

	public void imprimir() {

		NumberFormat formatter;
		formatter = DecimalFormat.getCurrencyInstance(new Locale("pt", "BR"));
		formatter.setMinimumFractionDigits(2);

		recuperarHistorico();

		for (String linha : vetOperacoes) {

			String buffer[] = linha.split(" ");

			// 0010 0000010 16/05/2008 - 07:09:57 - Saque caixa R$ 20,00

			for (int i = 0; i < buffer.length; i++) {

				switch (i) {
				case 0: // Agencia
				case 1: // Conta
					System.out.print(buffer[i] + " ");
					break;
				case 2: // Dia
				case 3: // Mes
					System.out.print(buffer[i] + "/");
					break;
				case 4: // Ano
				case 7: // Segundo
					System.out.print(buffer[i] + " - ");
					break;
				case 5: // Hora
				case 6: // Minuto
					System.out.print(buffer[i] + ":");
					break;
				case 8: // Saque ou deposito
					switch (buffer[i]) {
					case "001":
						System.out.print("Saque caixa  ");
						break;
					case "002":
						System.out.print("Deposito dinheiro  ");
						break;
					default:
						System.out.print("Opera��o Invalida  ");
					}
					break;
				case 9: // valor
					double tmp = Double.parseDouble(buffer[i].replace(",", "."));
					System.out.print(formatter.format(tmp));
					System.out.println();
					break;
				}

			}

		}

	}

	public void recuperarHistorico() {
		FileReader tArq1;
		BufferedReader tArq2;
		
		vetOperacoes = new Vector<String>();
		
		String tLinha = null;
		try {
			// Opera��o I - Abrir o arquivo
			tArq1 = new FileReader(numAge + "." + numConta + ".hist");
			tArq2 = new BufferedReader(tArq1);
			// Opera��o III - Ler atributo/valor e colocar na matriz
			while (true) {
				tLinha = tArq2.readLine();
				if (tLinha == null)
					break;
				// Criar vetOperacoes como um atributo do tipo Vector
				vetOperacoes.add(tLinha);
			}
			// Opera��o IV - Fechar o arquivo
			tArq2.close();
		} catch (FileNotFoundException e) {
			System.out.println("\n Conta sem movimento \n\n");
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
		}
	}

	public void gravar(int codHist, double valor) {
		FileWriter tArq1;
		PrintWriter tArq2;

		NumberFormat formatter;

		try {
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter(numAge + "." + numConta + ".hist", true);
			tArq2 = new PrintWriter(tArq1);
			Date hoje = new Date();
			Calendar cal = new GregorianCalendar();
			cal.setTime(hoje);
			dia = cal.get(Calendar.DAY_OF_MONTH);
			// O m�s em Java inicia com 0
			mes = cal.get(Calendar.MONTH) + 1;
			ano = cal.get(Calendar.YEAR);
			hora = cal.get(Calendar.HOUR);
			min = cal.get(Calendar.MINUTE);
			seg = cal.get(Calendar.SECOND);
			this.codHist = codHist;
			this.valor = valor;

			// GGGG NNNNNNN DD MM AAAA HH MM SS XXX VVVVVVVVVVV.VV
			formatter = new DecimalFormat("0000");
			tArq2.print(formatter.format(numAge) + " ");
			formatter = new DecimalFormat("0000000");
			tArq2.print(formatter.format(numConta) + " ");
			formatter = new DecimalFormat("00");
			tArq2.print(formatter.format(dia) + " ");
			tArq2.print(formatter.format(mes) + " ");
			formatter = new DecimalFormat("0000");
			tArq2.print(formatter.format(ano) + " ");
			formatter = new DecimalFormat("00");
			tArq2.print(formatter.format(hora) + " ");
			tArq2.print(formatter.format(min) + " ");
			tArq2.print(formatter.format(seg) + " ");
			formatter = new DecimalFormat("000");
			tArq2.print(formatter.format(this.codHist) + " ");
			formatter = new DecimalFormat("000000000.00");
			tArq2.println(formatter.format(this.valor));

			// Opera��o III - Fechar o arquivo
			tArq2.close();
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
		}
	}

}
