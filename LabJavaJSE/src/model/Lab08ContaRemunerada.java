package model;

import util.MyClassException;

public class Lab08ContaRemunerada extends Lab08ContaCorrenteEspecial implements Lab08ContaCorrenteInterface {

	public Lab08ContaRemunerada(int numAge, int numConta) throws MyClassException {
		super(numAge, numConta);
	}
	
	public void calcularJuros(){
		setSaldo(getSaldo()+(getSaldo()*(TAXA_JUROS/100)));
	}

}
