package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Lab05ContaCorrenteEspecial extends Lab03ContaCorrente {

	private double limiteCredito;

	public Lab05ContaCorrenteEspecial() {
		super();
	}

	public Lab05ContaCorrenteEspecial(int agencia, int conta, double saldo2, double limite,
			String nomeCliente) {
		super(agencia, conta, saldo2, nomeCliente);
		setLimiteCredito(limite);
		gravar();
	}

	public Lab05ContaCorrenteEspecial(int numAge, int numConta) {
		super(numAge, numConta);
		recuperar();
	}

	public double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public boolean removerArquivo() {
		super.removerArquivo();
		File tArq1;
		tArq1 = new File(getNumAge() + "." + getNumConta() + ".esp");
		tArq1.delete();
		return true;
	}

	public void imprimir() {
		super.imprimir();

		NumberFormat formatter;
		formatter = DecimalFormat.getCurrencyInstance(new Locale("pt", "BR"));
		formatter.setMinimumFractionDigits(2);
		System.out.println("Limite  : " + formatter.format(limiteCredito));
		System.out.println();
	}

	public int sacar(double p_valor) {

		if (((getSaldo() + getLimiteCredito()) >= p_valor) && (p_valor > 0)) {

			setSaldo(getSaldo() - p_valor);

			return 1;
		}

		return 0;
	}
	
	
	protected void recuperar() {
		
		super.recuperar();
		FileReader tArq1;
		BufferedReader tArq2;
		int tQtde = 4;
		try {
			// Opera��o I - Abrir o arquivo
			tArq1 = new FileReader(getNumAge() + "." + getNumConta() + ".esp");
			tArq2 = new BufferedReader(tArq1);
			// Opera��o II - Ler atributo/valor e colocar na matriz
			String[] tLinha = new String[tQtde];
			for (int i = 0; i < tQtde; i++) {
				tLinha[i] = tArq2.readLine();
			}
			// Opera��o III - Fechar o arquivo
			tArq2.close();
			setLimiteCredito(Double.parseDouble(tLinha[0]));
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
		}
	}

	public boolean gravar() {
		super.gravar();
		FileWriter tArq1;
		PrintWriter tArq2;
		try {
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter(getNumAge() + "." + getNumConta() + ".esp");
			tArq2 = new PrintWriter(tArq1);
			tArq2.println(getLimiteCredito());
			// Opera��o II - Fechar o arquivo
			tArq2.close();
			return true;
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
			return false;
		}

	}	

}
