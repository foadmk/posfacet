package model;

public class Lab02ContaCorrente {

	int numAge;
	int numConta;
	String nome;
	double saldo;

	public int sacar(double p_valor) {

		if ((saldo >= p_valor) && (p_valor > 0)) {
			saldo -= p_valor;
			return 1;
		}

		return 0;

	}

	public void depositar(double p_valor) {
		if (p_valor > 0) {
			saldo += p_valor;
		}
	}

	void Lab02ContaCorrente() {

	}

	void Lab02ContaCorrente(int numAge, int numConta) {
		this.numAge = numAge;
		this.numConta = numConta;
	}

	void Lab02ContaCorrente(int numAge, int numConta, double saldo, String nome) {
		this.numAge = numAge;
		this.numConta = numConta;
		this.nome = nome;
		this.saldo = saldo;
	}

	public void imprimir() {
		System.out.println("Nome: " + nome);
		System.out.println("Ag�ncia: " + numAge + "  Conta: " + numConta);
		System.out.printf("Saldo: R$%.2f\n", saldo);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumAge() {
		return numAge;
	}

	public void setNumAge(int numAge) {
		this.numAge = numAge;
	}

	public int getNumConta() {
		return numConta;
	}

	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
}
