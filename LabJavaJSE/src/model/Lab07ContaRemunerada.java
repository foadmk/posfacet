package model;

public class Lab07ContaRemunerada extends Lab05ContaCorrenteEspecial implements Lab07ContaCorrenteInterface {

	public Lab07ContaRemunerada(int numAge, int numConta) {
		super(numAge, numConta);
	}
	
	public void calcularJuros(){
		setSaldo(getSaldo()+(getSaldo()*(TAXA_JUROS/100)));
	}

}
