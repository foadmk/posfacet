package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import util.MyClassException;

public class Lab08ContaCorrenteEspecial extends Lab08ContaCorrente {

	private static double limite = 0.0;

	public Lab08ContaCorrenteEspecial(int numAge, int numConta, String nome, double saldo, double limite) throws MyClassException {
		super(numAge, numConta, nome, saldo);
		this.limite = limite;
	}
	
	public Lab08ContaCorrenteEspecial(int numAge, int numConta) throws MyClassException {
		super(numAge, numConta);
	}

	public double getLimite() {
		return this.limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}
	
	public void sacar(double valor) throws MyClassException{
		if(valor > (getSaldo()+limite)){
			MyClassException myObj = new MyClassException();
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem("Saldo n�o dispon�vel na conta.");
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		} 
		setSaldo(getSaldo() - valor);
	}
	
	public boolean removerArquivo () {
		super.removerArquivo();
		File tArq1;
		tArq1 = new File (getNumAge() + "." + getNumConta() + ".esp");
		tArq1.delete();
		return true;
	}
	
	public void imprimir() {
		super.imprimir();
		NumberFormat formatter;
		formatter = DecimalFormat.getCurrencyInstance(new Locale("pt","BR"));
		formatter.setMinimumFractionDigits(2);
		System.out.println ("Limite: " + formatter.format(getLimite()));
	}
	
	
	
	protected void recuperar () throws MyClassException{
		
		super.recuperar();
		
		FileReader     tArq1;
		BufferedReader tArq2;
		try
		{
			// Opera��o I - Abrir o arquivo
			tArq1 = new FileReader (getNumAge() + "." + getNumConta() + ".esp");
			tArq2 = new BufferedReader (tArq1);
			
			// Opera��o II - Ler atributo/valor 
			String limite = tArq2.readLine();
			
			// Opera��o III - Fechar o arquivo
			tArq2.close();
			this.setLimite(Double.parseDouble(limite));
				
		}
		catch (IOException tExcept)
		{
			tExcept.printStackTrace();
		}

	}
	
	public boolean gravar () throws MyClassException {
		
		super.gravar();
		
		FileWriter      tArq1;
		PrintWriter     tArq2;
		try
		{
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter (getNumAge() + "." + getNumConta() + ".esp");
			tArq2 = new PrintWriter (tArq1);
			double _lim = getLimite();
			tArq2.println(_lim);
			
			// Opera��o II - Fechar o arquivo
			tArq2.close();
			return true;
		}
		catch (IOException tExcept)
		{
			tExcept.printStackTrace();
			return false;
		}
		
	}

}
