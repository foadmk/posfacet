package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;

public class Lab07Historico {
	
	private int numAge;
	private int numConta;
	private int dia;
	private int mes;
	private int ano;
	private int hora;
	private int min;
	private int seg;
	private int codHist;
	private double valor;
	private Vector vetOperacoes = new Vector();
	
	
	public Lab07Historico(int numAge, int numConta) {
		this.numAge = numAge;
		this.numConta = numConta;
	}
	
	
	public boolean gravar(int p_hist, double p_valor) {
		FileWriter tArq1;
		PrintWriter tArq2;
		try {
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter(numAge + "." + numConta + ".hist",true);
			tArq2 = new PrintWriter(tArq1);
			Date hoje = new Date ();
			Calendar cal = new GregorianCalendar();
			cal.setTime(hoje);
			dia = cal.get(Calendar.DAY_OF_MONTH);
			// O m�s em Java inicia com 0
			mes = cal.get(Calendar.MONTH) + 1;
			ano = cal.get(Calendar.YEAR);
			hora = cal.get(Calendar.HOUR);
			min = cal.get(Calendar.MINUTE);
			seg = cal.get(Calendar.SECOND);	
			tArq2.print(numAge   + " ");
			tArq2.print(numConta + " ");
			tArq2.print(dia		+ " ");
			tArq2.print(mes		+ " ");
			tArq2.print(ano		+ " ");
			tArq2.print(hora		+ " ");
			tArq2.print(min		+ " ");
			tArq2.print(seg		+ " ");
			tArq2.print(p_hist	+ " ");
			tArq2.println(p_valor);
			// Opera��o III - Fechar o arquivo
			tArq2.close();
			return true;
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
			return false;
		}
	}

	
	public void imprimir() {
		recuperarHistorico();
		for(int i=0; i<vetOperacoes.size(); i++){
			String linha = (String)vetOperacoes.get(i);
			String[] separados = linha.split(" ");
			
			StringBuilder txt = new StringBuilder("");
			
			// Ag�ncia: 
			NumberFormat formatter;
			formatter = new DecimalFormat("0000");	
			String numAge = String.valueOf(formatter.format (Integer.parseInt(separados[0])));

			// Conta: 
			formatter = new DecimalFormat("0000000000");
			String numConta = separados[1];
			
			// Data:
			formatter = new DecimalFormat("00");
			String dia = String.valueOf(formatter.format (Integer.parseInt(separados[2])));
			String mes = String.valueOf(formatter.format (Integer.parseInt(separados[3])));
			String data = dia+'/'+mes+'/'+separados[4];
			
			// Hor�rio: 
			formatter = new DecimalFormat("00");
			String hora 	= String.valueOf(formatter.format (Integer.parseInt(separados[5])));
			String minuto 	= String.valueOf(formatter.format (Integer.parseInt(separados[6])));
			String segundo 	= String.valueOf(formatter.format (Integer.parseInt(separados[7])));
			String horario = hora+":"+minuto+":"+segundo;
			
			// Valor: 
			NumberFormat formatoMoeda = DecimalFormat.getCurrencyInstance(new Locale("pt","BR"));
			formatoMoeda.setMinimumFractionDigits(2);
			String valor = formatoMoeda.format(Double.parseDouble(separados[9]));

			
			// Descri��o:
			String descricao;
			if(separados[8].equals("1")){
				descricao = "Saque caixa R$ "+valor;
			} else if(separados[8].equals("2")){ 
				descricao = "Dep�sito dinheiro R$ "+valor;
			} else {
				descricao = "Atualiza��o do Saldo";
			}
			
			// Concatenar e exibir mensagem: 
			txt.append(numAge+" ");
			txt.append(numConta+" ");
			txt.append(data+" - ");
			txt.append(horario+" - ");
			txt.append(descricao);
			System.out.println(txt);
		}
	}
	
	public void recuperarHistorico() {
		FileReader 		tArq1;
		BufferedReader 	tArq2;
		String 			tLinha = null;
		try {
			// Opera��o I - Abrir o arquivo
			tArq1 = new FileReader(numAge + "." + numConta + ".hist");
			tArq2 = new BufferedReader(tArq1);
			// Opera��o III - Ler atributo/valor e colocar na matriz
			while (true) {
				tLinha = tArq2.readLine();
				if (tLinha == null)
					break;
				// Criar vetOperacoes como um atributo do tipo Vector
				vetOperacoes.add(tLinha);
			}
			// Opera��o IV - Fechar o arquivo
			tArq2.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("\n Conta sem movimento \n\n");
		}
		catch (IOException tExcept) 
		{
			tExcept.printStackTrace();
		}
	}

}
