package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import util.MyClassException;

public class Lab08ContaCorrente {

	private int numAge;
	private int numConta;
	private String nome;
	private double saldo;

	public Lab08ContaCorrente(){
		super();
	}

	public Lab08ContaCorrente(int numAge, int numConta) throws MyClassException{
		this.setNumAge(numAge);
		this.setNumConta(numConta);
		this.recuperar();
	}

	public Lab08ContaCorrente(int numAge, int numConta, String nome, double saldo) throws MyClassException{
		this.setNumAge(numAge);
		this.setNumConta(numConta);
		this.setNome(nome);
		this.setSaldo(saldo);
	}


	public int getNumAge() {
		return numAge;
	}

	public void setNumAge(int numAge) throws MyClassException {
		if(numAge < 0 || numAge > 9999){
			MyClassException myObj = new MyClassException();
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem("N�mero de ag�ncia inv�lida (deve ser entre 0001 e 9999)");
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		}
		this.numAge = numAge;
	}

	public int getNumConta() {
		return numConta;
	}

	public void setNumConta(int numConta) throws MyClassException {
		if(numConta < 0 || numConta > 9999999){
			MyClassException myObj = new MyClassException();
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem("N�mero de conta inv�lida (deve ser entre 0000001 e 9999999)");
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		}
		this.numConta = numConta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public void sacar(double valor) throws MyClassException{
		if(valor > saldo){
			MyClassException myObj = new MyClassException();
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem("Saldo n�o dispon�vel na conta.");
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		} else {
			saldo -= valor;
		}
	}

	public void depositar(double valor){
		saldo += valor;
	}


	public void imprimir() {
		System.out.println("\n\nCONSULTA ====================");
		System.out.println("Ag�ncia: "+numAge);
		System.out.println("Conta: "+numConta);
		System.out.println("Nome: "+nome);
		
		NumberFormat formatter;
		formatter = DecimalFormat.getCurrencyInstance(new Locale("pt","BR"));
		formatter.setMinimumFractionDigits(2);
		System.out.println ("Saldo: " + formatter.format(saldo));
	}



	protected void recuperar () throws MyClassException{
		FileReader     tArq1;
		BufferedReader tArq2;
		int            tQtde = 4;
		try {
			// Opera��o I - Abrir o arquivo
			tArq1 = new FileReader (getNumAge() + "." + getNumConta() + ".dat");
			tArq2 = new BufferedReader (tArq1);
			// Opera��o II - Ler atributo/valor e colocar na matriz
			String [] tLinha = new String [tQtde];
			for (int i = 0; i < tQtde; i++){
				tLinha [i] = tArq2.readLine();
			}
			// Opera��o III - Fechar o arquivo
			tArq2.close();
			setNumAge(Integer.parseInt(tLinha [0]));
			setNumConta(Integer.parseInt(tLinha [1]));
			setNome(tLinha [2]);
			setSaldo(Double.parseDouble(tLinha [3]));	
		} catch (IOException tExcept)	{
			MyClassException myObj = new MyClassException();
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem(tExcept.getMessage());
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		}

	}
	
	public boolean gravar() throws MyClassException {
		FileWriter      tArq1;
		PrintWriter     tArq2;
		try {
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter (getNumAge() + "." + getNumConta() + ".dat");
			tArq2 = new PrintWriter (tArq1);
			tArq2.println (getNumAge());
			tArq2.println (getNumConta());
			tArq2.println (getNome());
			tArq2.println (getSaldo());
			// Opera��o II - Fechar o arquivo
			tArq2.close();
			return true;
		}
		catch (IOException tExcept) {
			MyClassException myObj = new MyClassException();
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem(tExcept.getMessage());
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		}
		
	}
	
	
	public boolean removerArquivo () {
		File      tArq1;
		tArq1 = new File (numAge + "." + numConta + ".dat");
		tArq1.delete();
		tArq1 = new File(numAge + "." + numConta + ".hist");
		tArq1.delete();
		return true;
	}


}
