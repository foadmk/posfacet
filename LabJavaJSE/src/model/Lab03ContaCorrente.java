package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Lab03ContaCorrente {

	private int numAge;
	private int numConta;
	private String nome;
	private double saldo;

	public static boolean valNumAge(int agencia)
	{
		return (agencia > 0) && (agencia < 10000);
	}
	
	public int sacar(double p_valor) {

		if ((saldo >= p_valor) && (p_valor > 0)) {
			saldo -= p_valor;
			return 1;
		}

		return 0;

	}

	protected void recuperar() {
		FileReader tArq1;
		BufferedReader tArq2;
		int tQtde = 4;
		try {
			// Opera��o I - Abrir o arquivo
			tArq1 = new FileReader(getNumAge() + "." + getNumConta() + ".dat");
			tArq2 = new BufferedReader(tArq1);
			// Opera��o II - Ler atributo/valor e colocar na matriz
			String[] tLinha = new String[tQtde];
			for (int i = 0; i < tQtde; i++) {
				tLinha[i] = tArq2.readLine();
			}
			// Opera��o III - Fechar o arquivo
			tArq2.close();
			setNumAge(Integer.parseInt(tLinha[0]));
			setNumConta(Integer.parseInt(tLinha[1]));
			setNome(tLinha[2]);
			setSaldo(Double.parseDouble(tLinha[3]));
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
		}
	}

	public boolean gravar() {
		FileWriter tArq1;
		PrintWriter tArq2;
		try {
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter(getNumAge() + "." + getNumConta() + ".dat");
			tArq2 = new PrintWriter(tArq1);
			tArq2.println(getNumAge());
			tArq2.println(getNumConta());
			tArq2.println(getNome());
			tArq2.println(getSaldo());
			// Opera��o II - Fechar o arquivo
			tArq2.close();
			return true;
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
			return false;
		}

	}

	public void depositar(double p_valor) {
		if (p_valor > 0) {
			saldo += p_valor;
			
		}
	}

	public Lab03ContaCorrente() {

	}

	public Lab03ContaCorrente(int numAge, int numConta) {
		this.numAge = numAge;
		this.numConta = numConta;
		recuperar();
	}
	

	public Lab03ContaCorrente(int agencia, int conta, double saldo2,
			String nomeCliente) {
		this.numAge = agencia;
		this.numConta = conta;
		this.nome = nomeCliente;
		this.saldo = saldo2;
	}	

	public void imprimir() {
		NumberFormat formatter;
		formatter = DecimalFormat.getCurrencyInstance(new Locale("pt","BR"));
		formatter.setMinimumFractionDigits(2);

		
		System.out.println("Nome    : " + nome);
		System.out.println();
		System.out.printf ("Ag�ncia : %04d\n\nConta   : %07d\n", numAge, numConta);
		System.out.println();
		System.out.println("Saldo   : " + formatter.format(saldo));	
		System.out.println();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumAge() {
		return numAge;
	}

	public void setNumAge(int numAge) {
		this.numAge = numAge;
	}

	public int getNumConta() {
		return numConta;
	}

	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public static boolean valNumConta(int conta) {
		return ((conta > 0) || (conta < 10000000));
	}
	
	
	public boolean removerArquivo () 
	{
		File      tArq1;
		tArq1 = new File (numAge + "." + numConta + ".dat");
		tArq1.delete();
		tArq1 = new File(numAge + "." + numConta + ".hist");
		tArq1.delete();
		return true;
	}

}
