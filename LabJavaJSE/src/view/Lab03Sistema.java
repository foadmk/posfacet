package view;

import java.util.Scanner;

import model.Lab03ContaCorrente;

public class Lab03Sistema {

	Lab03ContaCorrente objContaCorrente;

	public static int entradaAgencia() {
		int agencia;
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		// Ag�ncia
		do {
			System.out.print("Numero da Agencia : ");
			agencia = s.nextInt();
		} while (!Lab03ContaCorrente.valNumAge(agencia));

		return agencia;
	}

	public static int entradaConta() {
		int conta;
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		// Ag�ncia
		do {
			System.out.print("Numero da Conta : ");
			conta = s.nextInt();
		} while (!Lab03ContaCorrente.valNumConta(conta));

		return conta;
	}

	public static double entradaValor(String operacao) {
		double valor;
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		do {
			System.out.print("Digite o valor do " + operacao + ": ");
			valor = s.nextDouble();
		} while (valor <= 0.0);

		return valor;
	}

	public static boolean entradaConfirmacao(String operacao) {

		char confirma;
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		do {
			System.out.print("Confirma " + operacao + " (S/N) : ");
			confirma = s.next().charAt(0);
		} while (!(confirma == 'n' || confirma == 'N' || confirma == 's' || confirma == 'S'));

		if (confirma == 's' || confirma == 'S') {
			return true;
		}

		return false;
	}

	public void execSaque() {

		int agencia;
		int conta;
		double valor;

		System.out.println();
		System.out.println("Saque"); // Mensagem inicial
		System.out.println();

		// Ag�ncia
		agencia = entradaAgencia();

		// Conta
		conta = entradaConta();

		objContaCorrente = new Lab03ContaCorrente(agencia, conta);
		objContaCorrente.imprimir();

		// Valor

		valor = entradaValor("Saque");
		
		if (entradaConfirmacao("Saque")) {

			if (objContaCorrente.sacar(valor) == 1) {
				objContaCorrente.gravar();
				System.out.println("Saque realizado!");
			} else {
				System.out.println("Saldo insuficiente!");
			}

		} else {
			System.out.println("Saque cancelado.");
		}
	}

	public void execDeposito() {

		int agencia;
		int conta;
		double valor;

		System.out.println();
		System.out.println("Saque"); // Mensagem inicial
		System.out.println();

		// Ag�ncia
		agencia = entradaAgencia();

		// Conta
		conta = entradaConta();

		objContaCorrente = new Lab03ContaCorrente(agencia, conta);
		objContaCorrente.imprimir();

		// Valor

		valor = entradaValor("Deposito");
		
		if (entradaConfirmacao("Deposito")) {

			objContaCorrente.depositar(valor);
			objContaCorrente.gravar();
			System.out.println("Deposito realizado.");
		} else {
			System.out.println("Deposito cancelado.");
		}

	}

	public void execConsulta() {

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println();
		System.out.println("Consulta Saldo"); // Mensagem inicial
		System.out.println();

		int agencia = entradaAgencia();
		int conta = entradaConta();

		objContaCorrente = new Lab03ContaCorrente(agencia, conta);
		objContaCorrente.imprimir();
	}

	public void execConsulta(int agencia, int conta) {

		System.out.println();
		System.out.println("Consulta Saldo"); // Mensagem inicial
		System.out.println();

		objContaCorrente = new Lab03ContaCorrente(agencia, conta);
		objContaCorrente.imprimir();
	}

	
	public void execCadastramento() {

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println();
		System.out.println("Cadastramento"); // Mensagem inicial
		System.out.println();

		// Ag�ncia
		int agencia = entradaAgencia();

		// Conta
		int conta = entradaConta();

		// Nome
		System.out.print("Nome do Cliente   : ");
		String nomeCliente = s.next();

		// Valor
		double saldo = entradaValor("Saldo");

		if(entradaConfirmacao("Cadastramento")){
			objContaCorrente = new Lab03ContaCorrente(agencia, conta, saldo,
					nomeCliente);
			objContaCorrente.gravar();

			System.out.println("Cadastramento realizado.");
		} else {
			System.out.println("Cadastramento cancelado.");
		}

	}

	public static void main(String[] args) {

		Lab03Sistema objPrincipal = new Lab03Sistema();
		boolean run = true;

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		while (run) {
			System.out.println();
			System.out.println("Menu Principal");
			System.out.println();
			System.out.println("1 - Cadastramento");
			System.out.println();
			System.out.println("2 - Saque");
			System.out.println();
			System.out.println("3 - Deposito");
			System.out.println();
			System.out.println("4 - Consulta");
			System.out.println();
			System.out.println("9 - Fim");
			System.out.println();
			System.out.print("Op��o: ");
			int opcao = s.nextInt();
			System.out.println();

			switch (opcao) {
			case 1:
				objPrincipal.execCadastramento();
				break;
			case 2:
				objPrincipal.execSaque();
				break;
			case 3:
				objPrincipal.execDeposito();
				break;
			case 4:
				objPrincipal.execConsulta();
				break;
			case 9:
				run = false;
				break;
			default:
				System.out.println("Op��o Inv�lida!");
			}

		}

		System.out.print("At� Logo!");

	}
}
