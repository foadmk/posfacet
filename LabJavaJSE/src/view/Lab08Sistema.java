package view;

import java.io.IOException;
import java.util.Scanner;

import util.MyClassException;
import model.Lab08ContaCorrente;
import model.Lab08Historico;
import model.Lab08ContaCorrenteEspecial;
import model.Lab08ContaRemunerada;
import model.Lab08Historico;
import model.Lab08Historico;

public class Lab08Sistema {
	
	public static void main(String[] args) throws MyClassException {

		try{
			
			int opcao_escolhida;
			
			do{
				opcao_escolhida = exibirMenu();
				switch(opcao_escolhida) {
				case 1:
					execCadastramento();
					break;
				case 2:
					execSaque();
					break;
				case 3:
					execDeposito();
					break;
				case 4:
					execConsulta();
					break;
				case 5:
					execExtrato();
					break;
				case 6:
					execAtualizarSaldo();
					break;
				case 8:
					execRemoverContaCorrente();
					break;
				case 9:
					break;
				default:
					System.out.println("\nOp��o inv�lida.");
					break;
				}
			} while(opcao_escolhida != 9);
		
			System.out.println("\n\nObrigado por utilizar nossos servi�os.");
		} catch (MyClassException e){
			System.out.println ("Classe: " + e.getClasse());
			System.out.println ("Mensagem Objeto: " + e.getMessage());
			System.out.println ("Mensagem Negocio: " + e.getMensagem());
			
		}
	}
	
	private static void execCadastramento() throws MyClassException {
		
		Scanner metodoEntrada = new Scanner(System.in);

		System.out.println("\n\nCADASTRO ==================");
		
		System.out.print(" Numero da Ag�ncia :");
		int numAge = Integer.parseInt(metodoEntrada.nextLine());
		
		System.out.print(" Numero da Conta   :");
		int numConta = Integer.parseInt(metodoEntrada.nextLine());
		
		System.out.print(" Nome do Cliente   :");
		String nome = metodoEntrada.nextLine();
		
		double saldo = 0;
		do {
			System.out.print(" Saldo             :");
			saldo = metodoEntrada.nextDouble();
		} while (saldo <= 0.0);
		
		double limite = 0;
		if(numAge >= 5000){
			do {
				System.out.print(" Limite            :");
				limite = metodoEntrada.nextDouble();
			} while (limite <= 0.0);
		}
		
		System.out.print(" Confirmar cadastramento (s/n):");
		String confirmacao = metodoEntrada.next();
		
		if(confirmacao.contentEquals("s") || confirmacao.contentEquals("S")) {
			if(numAge >= 5000){
				Lab08ContaCorrenteEspecial contaCorrenteEspecial = new Lab08ContaCorrenteEspecial(numAge, numConta, nome, saldo, limite);
				contaCorrenteEspecial.gravar();
			} else {
				Lab08ContaCorrente contaCorrente = new Lab08ContaCorrente(numAge, numConta, nome, saldo);
				contaCorrente.gravar();
			}
			System.out.println("\n� CADASTRO REALIZADO.");
		} else {
			System.out.println("\n� CADASTRO CANCELADO.");
		}
		
		
	}

	private static void execSaque() throws MyClassException {
		
		Scanner metodoEntrada = new Scanner(System.in);

		System.out.println("\n\nSAQUE ====================");
		
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		double valorSaque;
		do {
			System.out.print(" Valor do saque:");
			valorSaque = metodoEntrada.nextDouble();
		} while (valorSaque <= 0.0);

		System.out.print(" Confirmar saque (s/n):");
		String confirmacao = metodoEntrada.next();
		
		if(confirmacao.contentEquals("s") || confirmacao.contentEquals("S")) {
			
			Lab08ContaCorrente contaCorrente;
			if(numAge >= 5000){
				contaCorrente = new Lab08ContaCorrenteEspecial(numAge, numConta);
			} else {
				contaCorrente = new Lab08ContaCorrente(numAge, numConta);
			}
			
			// Sacar:
			contaCorrente.sacar(valorSaque);
			
			// Salvar: 
			contaCorrente.gravar();
			System.out.println("\n� SAQUE REALIZADO.");
			
			// Gravar hist�rico:
			Lab08Historico historico = new Lab08Historico(numAge, numConta);
			historico.gravar(1, valorSaque);
			
		} else {
			
			MyClassException myObj = new MyClassException();
			myObj.setClasse("Lab08Sistema");
			myObj.setMensagem("Saldo n�o dispon�vel na conta.");
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote("view");
			throw myObj;
		}
		
	}
	
	private static void execConsulta() throws MyClassException {
		
		Scanner metodoEntrada = new Scanner(System.in);
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		Lab08ContaCorrente contaCorrente;
		if(numAge >= 5000){
			contaCorrente = new Lab08ContaCorrenteEspecial(numAge, numConta);
		} else {
			contaCorrente = new Lab08ContaCorrente(numAge, numConta);
		}
		contaCorrente.imprimir();
	}
	
	private static void execDeposito() throws MyClassException {
		
		Scanner metodoEntrada = new Scanner(System.in);

		System.out.println("\n\nDEP�SITO ====================");
		
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		System.out.print(" Valor do Dep�sito :");
		double valorSaque = metodoEntrada.nextDouble();
		
		System.out.print(" Confirmar dep�sito (s/n):");
		String confirmacao = metodoEntrada.next();
		
		if(confirmacao.contentEquals("s") || confirmacao.contentEquals("S")) {

			// Depositar e salvar:
			Lab08ContaCorrente contaCorrente;
			if(numAge >= 5000){
				contaCorrente = new Lab08ContaCorrenteEspecial(numAge, numConta);
			} else {
				contaCorrente = new Lab08ContaCorrente(numAge, numConta);
			}
			contaCorrente.depositar(valorSaque);
			contaCorrente.gravar();
			
			// Gravar hist�rico:
			Lab08Historico historico = new Lab08Historico(numAge, numConta);
			historico.gravar(2, valorSaque);

			System.out.println("\n� DEPOSITO REALIZADO.");
		} else {
			System.out.println("\n� DEPOSITO CANCELADO.");
		}
		
	}
	
	private static void execExtrato() throws MyClassException {
		
		// Pedir dados: =========================================
		Scanner metodoEntrada = new Scanner(System.in);
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		// Imprimir pela contaCorrente: =========================
		Lab08ContaCorrente contaCorrente;
		if(numAge >= 5000){
			contaCorrente = new Lab08ContaCorrenteEspecial(numAge, numConta);
		} else {
			contaCorrente = new Lab08ContaCorrente(numAge, numConta);
		}
		contaCorrente.imprimir();
		
		// Imprimir hist�rico: ==================================
		Lab08Historico historico = new Lab08Historico(numAge, numConta);
		historico.imprimir();
	}
	
	private static void execRemoverContaCorrente() throws MyClassException {

		Scanner metodoEntrada = new Scanner(System.in);
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		Lab08ContaCorrente contaCorrente;
		if(numAge >= 5000){
			contaCorrente = new Lab08ContaCorrenteEspecial(numAge, numConta);
		} else {
			contaCorrente = new Lab08ContaCorrente(numAge, numConta);
		}
		contaCorrente.removerArquivo();
	}
	
	private static void execAtualizarSaldo() throws MyClassException {
		
		Scanner metodoEntrada = new Scanner(System.in);
		
		System.out.println("\n\nATUALIZAR SALDO =============");
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		if(numAge > 5000){
			Lab08ContaRemunerada contaCorrente = new Lab08ContaRemunerada(numAge, numConta); 
			contaCorrente.calcularJuros();
			contaCorrente.gravar();
			contaCorrente.imprimir();
			
			Lab08Historico historico = new Lab08Historico(numAge, numConta);
			historico.gravar(3, contaCorrente.getSaldo());
		}
	}
	
	
	private static int exibirMenu() {	
		
		System.out.println("\n\nMENU =====================");
		System.out.println("  1. Cadastramento");
		System.out.println("  2. Saque");
		System.out.println("  3. Dep�sito");
		System.out.println("  4. Consulta");
		System.out.println("  5. Extrato");
		System.out.println("  6. Atualizar saldo");
		System.out.println("  8. Remove conta corrente");
		System.out.println("  9. Fim");
		System.out.print("\nDigite a op��o desejada:");
		
		Scanner metodoEntrada = new Scanner(System.in);
		int opcao_escolhida = metodoEntrada.nextInt();
		
		return opcao_escolhida;
	}
	
}
