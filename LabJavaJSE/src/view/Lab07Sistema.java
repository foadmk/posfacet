package view;

import java.util.Scanner;
import model.Lab03ContaCorrente;
import model.Lab04Historico;
import model.Lab05ContaCorrenteEspecial;
import model.Lab07ContaRemunerada;
import model.Lab07Historico;

public class Lab07Sistema {
	
	public static void main(String[] args) {

		int opcao_escolhida;
		
		do{
			opcao_escolhida = exibirMenu();
			switch(opcao_escolhida) {
			case 1:
				execCadastramento();
				break;
			case 2:
				execSaque();
				break;
			case 3:
				execDeposito();
				break;
			case 4:
				execConsulta();
				break;
			case 5:
				execExtrato();
				break;
			case 6:
				execAtualizarSaldo();
				break;
			case 8:
				execRemoverContaCorrente();
				break;
			case 9:
				break;
			default:
				System.out.println("\nOp��o inv�lida.");
				break;
			}
		} while(opcao_escolhida != 9);
		
		System.out.println("\n\nObrigado por utilizar nossos servi�os.");
	}
	
	private static void execCadastramento() {
		
		Scanner metodoEntrada = new Scanner(System.in);

		System.out.println("\n\nCADASTRO ==================");
		
		System.out.print(" Numero da Ag�ncia :");
		int numAge = Integer.parseInt(metodoEntrada.nextLine());
		
		System.out.print(" Numero da Conta   :");
		int numConta = Integer.parseInt(metodoEntrada.nextLine());
		
		System.out.print(" Nome do Cliente   :");
		String nome = metodoEntrada.nextLine();
		
		double saldo = 0;
		do {
			System.out.print(" Saldo             :");
			saldo = metodoEntrada.nextDouble();
		} while (saldo <= 0.0);
		
		double limite = 0;
		if(numAge >= 5000){
			do {
				System.out.print(" Limite            :");
				limite = metodoEntrada.nextDouble();
			} while (limite <= 0.0);
		}
		
		System.out.print(" Confirmar cadastramento (s/n):");
		String confirmacao = metodoEntrada.next();
		
		if(confirmacao.contentEquals("s") || confirmacao.contentEquals("S")) {
			if(numAge >= 5000){
				Lab05ContaCorrenteEspecial contaCorrenteEspecial = new Lab05ContaCorrenteEspecial(numAge, numConta, saldo, limite, nome);
				contaCorrenteEspecial.gravar();
			} else {
				Lab03ContaCorrente contaCorrente = new Lab03ContaCorrente(numAge, numConta, saldo, nome);
				contaCorrente.gravar();
			}
			System.out.println("\n� CADASTRO REALIZADO.");
		} else {
			System.out.println("\n� CADASTRO CANCELADO.");
		}
		
		
	}

	private static void execSaque() {
		
		Scanner metodoEntrada = new Scanner(System.in);

		System.out.println("\n\nSAQUE ====================");
		
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		double valorSaque;
		do {
			System.out.print(" Valor do saque:");
			valorSaque = metodoEntrada.nextDouble();
		} while (valorSaque <= 0.0);

		System.out.print(" Confirmar saque (s/n):");
		String confirmacao = metodoEntrada.next();
		
		if(confirmacao.contentEquals("s") || confirmacao.contentEquals("S")) {
			
			Lab03ContaCorrente contaCorrente;
			if(numAge >= 5000){
				contaCorrente = new Lab05ContaCorrenteEspecial(numAge, numConta);
			} else {
				contaCorrente = new Lab03ContaCorrente(numAge, numConta);
			}
			
			int sacou = contaCorrente.sacar(valorSaque);
			if(sacou == 1){
				
				// Salvar: 
				contaCorrente.gravar();
				System.out.println("\n� SAQUE REALIZADO.");
				
				// Gravar hist�rico:
				Lab04Historico historico = new Lab04Historico(numAge, numConta);
				historico.gravar(1, valorSaque);
				
			} else {
				System.out.println("\n� SALDO INSUFICIENTE.");
			}
			
		} else {
			System.out.println("\n� SAQUE CANCELADO.");
		}
		
	}
	
	private static void execConsulta() {
		
		Scanner metodoEntrada = new Scanner(System.in);
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		Lab03ContaCorrente contaCorrente;
		if(numAge >= 5000){
			contaCorrente = new Lab05ContaCorrenteEspecial(numAge, numConta);
		} else {
			contaCorrente = new Lab03ContaCorrente(numAge, numConta);
		}
		contaCorrente.imprimir();
	}
	
	private static void execDeposito() {
		
		Scanner metodoEntrada = new Scanner(System.in);

		System.out.println("\n\nDEP�SITO ====================");
		
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		System.out.print(" Valor do Dep�sito :");
		double valorSaque = metodoEntrada.nextDouble();
		
		System.out.print(" Confirmar dep�sito (s/n):");
		String confirmacao = metodoEntrada.next();
		
		if(confirmacao.contentEquals("s") || confirmacao.contentEquals("S")) {

			// Depositar e salvar:
			Lab03ContaCorrente contaCorrente;
			if(numAge >= 5000){
				contaCorrente = new Lab05ContaCorrenteEspecial(numAge, numConta);
			} else {
				contaCorrente = new Lab03ContaCorrente(numAge, numConta);
			}
			contaCorrente.depositar(valorSaque);
			contaCorrente.gravar();
			
			// Gravar hist�rico:
			Lab04Historico historico = new Lab04Historico(numAge, numConta);
			historico.gravar(2, valorSaque);

			System.out.println("\n� DEPOSITO REALIZADO.");
		} else {
			System.out.println("\n� DEPOSITO CANCELADO.");
		}
		
	}
	
	private static void execExtrato(){
		
		// Pedir dados: =========================================
		Scanner metodoEntrada = new Scanner(System.in);
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		// Imprimir pela contaCorrente: =========================
		Lab03ContaCorrente contaCorrente;
		if(numAge >= 5000){
			contaCorrente = new Lab05ContaCorrenteEspecial(numAge, numConta);
		} else {
			contaCorrente = new Lab03ContaCorrente(numAge, numConta);
		}
		contaCorrente.imprimir();
		
		// Imprimir hist�rico: ==================================
		Lab07Historico historico = new Lab07Historico(numAge, numConta);
		historico.imprimir();
	}
	
	private static void execRemoverContaCorrente() {

		Scanner metodoEntrada = new Scanner(System.in);
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		Lab03ContaCorrente contaCorrente;
		if(numAge >= 5000){
			contaCorrente = new Lab05ContaCorrenteEspecial(numAge, numConta);
		} else {
			contaCorrente = new Lab03ContaCorrente(numAge, numConta);
		}
		contaCorrente.removerArquivo();
	}
	
	private static void execAtualizarSaldo() {
		
		Scanner metodoEntrada = new Scanner(System.in);
		
		System.out.println("\n\nATUALIZAR SALDO =============");
		System.out.print(" Numero da Ag�ncia :");
		int numAge = metodoEntrada.nextInt();
		
		System.out.print(" Numero da Conta   :");
		int numConta = metodoEntrada.nextInt();
		
		if(numAge > 5000){
			Lab07ContaRemunerada contaCorrente = new Lab07ContaRemunerada(numAge, numConta); 
			contaCorrente.calcularJuros();
			contaCorrente.gravar();
			contaCorrente.imprimir();
			
			Lab07Historico historico = new Lab07Historico(numAge, numConta);
			historico.gravar(3, contaCorrente.getSaldo());
		}
	}
	
	
	private static int exibirMenu() {	
		
		System.out.println("\n\nMENU =====================");
		System.out.println("  1. Cadastramento");
		System.out.println("  2. Saque");
		System.out.println("  3. Dep�sito");
		System.out.println("  4. Consulta");
		System.out.println("  5. Extrato");
		System.out.println("  6. Atualizar saldo");
		System.out.println("  8. Remove conta corrente");
		System.out.println("  9. Fim");
		System.out.print("\nDigite a op��o desejada:");
		
		Scanner metodoEntrada = new Scanner(System.in);
		int opcao_escolhida = metodoEntrada.nextInt();
		
		return opcao_escolhida;
	}
	
}
