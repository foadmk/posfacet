package view;

import java.util.Scanner;

import model.Lab02ContaCorrente;

public class Lab02Sistema {

	Lab02ContaCorrente objContaCorrente = new Lab02ContaCorrente();

	public void execSaque() {

		int agencia;
		int conta;
		double valor;
		char confirma;

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println();
		System.out.println("Saque"); // Mensagem inicial
		System.out.println();

		// Ag�ncia
		System.out.print("Numero da Agencia : ");
		agencia = s.nextInt();

		// Conta
		System.out.print("Numero da Conta   : ");
		conta = s.nextInt();

		// Nome
		System.out.print("Nome do Cliente   : ");
		String nomeCliente = s.next();

		// Valor

		do {
			System.out.print("Digite o valor do saque: ");
			valor = s.nextDouble();
		} while (valor <= 0.0);

		do {
			System.out.print("Confirma saque (S/N) : ");
			confirma = s.next().charAt(0);
		} while (!(confirma == 'n' || confirma == 'N' || confirma == 's'
				|| confirma == 'S'));

		if (confirma == 's' || confirma == 'S') {

			if (objContaCorrente.sacar(valor) == 1) {
				System.out.println("Saque realizado!");
			} else {
				System.out.println("Saldo insuficiente!");
			}

		} else {
			System.out.println("Saque cancelado.");
		}
	}

	public void execDeposito() {

		double valor;
		int agencia;
		int conta;
		char confirma;
		String nomeCliente;

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println();
		System.out.println("Dep�sito"); // Mensagem inicial
		System.out.println();

		// Ag�ncia
		System.out.print("Numero da Agencia : ");
		agencia = s.nextInt();

		// Conta
		System.out.print("Numero da Conta   : ");
		conta = s.nextInt();

		// Nome
		System.out.print("Nome do Cliente   : ");
		nomeCliente = s.next();

		// Valor

		do {
			System.out.print("Digite o valor do deposito: ");
			valor = s.nextDouble();
		} while (valor <= 0.0);

		do {
			System.out.print("Confirma dep�sito (S/N) : ");
			confirma = s.next().charAt(0);
		} while (!(confirma == 'n' || confirma == 'N' || confirma == 's'
				|| confirma == 'S'));
		
		if (confirma == 's' || confirma == 'S') {

			objContaCorrente.depositar(valor);

			System.out.println("Deposito realizado.");
		} else {
			System.out.println("Deposito cancelado.");
		}

	}

	public void execConsulta() {
		objContaCorrente.imprimir();
	}

	public void execCadastramento() {

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println();
		System.out.println("Cadastramento"); // Mensagem inicial
		System.out.println();

		// Ag�ncia
		System.out.print("Numero da Agencia : ");
		int agencia = s.nextInt();

		// Conta
		System.out.print("Numero da Conta   : ");
		int conta = s.nextInt();

		// Nome
		System.out.print("Nome do Cliente   : ");
		String nomeCliente = s.next();

		// Valor
		System.out.print("Saldo             : ");
		double saldo = s.nextDouble();

		char confirma;
		do {
			System.out.print("Confirma cadastramento (S/N) : ");
			confirma = s.next().charAt(0);
		} while (!(confirma == 'n' || confirma == 'N' || confirma == 's' || confirma == 'S'));

		if (confirma == 's' || confirma == 'S') {
			objContaCorrente.setNumAge(agencia);
			objContaCorrente.setNumConta(conta);
			objContaCorrente.setNome(nomeCliente);
			objContaCorrente.setSaldo(saldo);

			System.out.println("Cadastramento realizado.");
		} else {
			System.out.println("Cadastramento cancelado.");
		}

	}

	public static void main(String[] args) {

		Lab02Sistema objPrincipal = new Lab02Sistema();
		boolean run = true;

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		while (run) {
			System.out.println();
			System.out.println("Menu Principal");
			System.out.println();
			System.out.println("1 - Cadastramento");
			System.out.println();
			System.out.println("2 - Saque");
			System.out.println();
			System.out.println("3 - Deposito");
			System.out.println();
			System.out.println("4 - Consulta");
			System.out.println();
			System.out.println("9 - Fim");
			System.out.println();
			System.out.print("Op��o: ");
			int opcao = s.nextInt();
			System.out.println();

			switch (opcao) {
			case 1:
				objPrincipal.execCadastramento();
				break;
			case 2:
				objPrincipal.execSaque();
				break;
			case 3:
				objPrincipal.execDeposito();
				break;
			case 4:
				objPrincipal.execConsulta();
				break;
			case 9:
				run = false;
				break;
			default:
				System.out.println("Op��o Inv�lida!");
			}

		}

		System.out.print("At� Logo!");

	}
}
