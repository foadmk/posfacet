package exercicios;

public class ContaCorrente {

	int conta;
	int agencia;
	double saldo;
	String nomeCliente;
	
	int sacar (double valor){
		if (saldo>valor){
			saldo -= valor;
			return 1;
		}
		return 0;
	}
	
	void depositar(double valor){
		saldo += valor;
	}
	
	void imprimir() {
		System.out.println("Nome: " + nomeCliente);
		System.out.println("Ag�ncia: " + agencia + "  Conta: "+conta);
		System.out.printf("Saldo: R$%.2f\n", saldo);
	}

}
