package exercicios;

import java.util.Scanner;

public class PrincipalContaCorrente {

	ContaCorrente objContaCorrente = new ContaCorrente();

	public void execSaque() {
		
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");
		System.out.print("Digite o valor do saque: ");
		double valor = s.nextDouble();
		
		if (objContaCorrente.sacar(valor) == 1) {
			System.out.println("Saque realizado!");
		} else {
			System.out.println("Saque n�o realizado!");
		}
		
	}

	public void execDeposito() {
	
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");
		System.out.print("Digite o valor do deposito: ");
		double valor = s.nextDouble();
		
		objContaCorrente.depositar(valor);
	}

	public void execConsulta() {
		objContaCorrente.imprimir();
	}

	public void execCadastrar() {

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Ola!"); // Mensagem inicial

		// Ag�ncia
		System.out.print("Digite n�mero da ag�ncia: ");
		int agencia = s.nextInt();
		objContaCorrente.agencia = agencia;

		// Conta
		System.out.print("Digite n�mero da conta: ");
		int conta = s.nextInt();
		objContaCorrente.conta = conta;

		// Nome
		System.out.print("Digite nome do cliente: ");
		String nomeCliente = s.next();
		objContaCorrente.nomeCliente = nomeCliente;

		// Valor
		System.out.print("Digite o saldo: "); 
		double saldo = s.nextDouble();
		objContaCorrente.saldo = saldo;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrincipalContaCorrente objPrincipal = new PrincipalContaCorrente();
		boolean run = true;
		
		Scanner s = new Scanner(System.in).useDelimiter("\r\n");
		
		while(run)
		{
			System.out.println("Sistema Banco");
			System.out.println(".");
			System.out.println("Digite 1 para Cadastro");
			System.out.println("Digite 2 para Consulta de Saldo");
			System.out.println("Digite 3 para Dep�sitos");
			System.out.println("Digite 4 para Saques");
			System.out.println("Digite 5 para Sair");
			System.out.println(".");
			System.out.print("Digite sua op��o: ");
			int opcao = s.nextInt();
			System.out.println(".");
			
			switch(opcao){
				case 1: 
					objPrincipal.execCadastrar();
					break;
				case 2:
					objPrincipal.execConsulta();
					break;
				case 3:
					objPrincipal.execDeposito();
					break;
				case 4:
					objPrincipal.execSaque();
					break;
				case 5:
					run = false;
					break;
				default:
					System.out.println("Op��o Inv�lida!");
					System.out.println(".");
			}
		
		}
		
		System.out.print("At� Logo!");
		
	}

}
