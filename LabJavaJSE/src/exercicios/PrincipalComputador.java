package exercicios;

public class PrincipalComputador {

	public static void main(String[] args) {
		Computador novoPC1 = new Computador();
		
		novoPC1.marca="HP";
		novoPC1.cor="Preto";
		novoPC1.modelo="386DX";
		novoPC1.numeroSerie="XADA234005";
		novoPC1.pre�o = 1000.00;
		
		novoPC1.imprimir();
		novoPC1.calculaValor();
		novoPC1.imprimir();

		Computador novoPC2 = new Computador();
		
		novoPC2.marca="IBM";
		novoPC2.cor="Branco";
		novoPC2.modelo="286DX";
		novoPC2.numeroSerie="XUNA358343";
		novoPC2.pre�o = 1000.00;
		
		novoPC2.calculaValor();
		novoPC2.imprimir();

		if (novoPC2.AlterarValor(999.00) == 1){
			System.out.println("Alterado");
		}
		else
		{
			System.out.println("N�o Alterado");
		}
		
		if (novoPC2.AlterarValor(-999.00) == 1){
			System.out.println("Valor Alterado");
		}
		else
		{
			System.out.println("Valor N�o Alterado");
		}
		
		novoPC2.imprimir();
		
	}

}
