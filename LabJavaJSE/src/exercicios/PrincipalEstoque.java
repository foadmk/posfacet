package exercicios;


public class PrincipalEstoque {

	public final int idCafe = 0;
	public final int idLeite = 1;
	public final int idAcucar = 2;	
	
	public Estoque[] produtos;
	
	
	public void inicializar() {
		produtos = new Estoque[3];
		
		produtos[idCafe] = new Estoque();		
		produtos[idCafe].nomeProduto = "Caf�";
		produtos[idCafe].valor = 0.50;
		produtos[idCafe].quantidade = 10;

		produtos[idLeite] = new Estoque();
		produtos[idLeite].nomeProduto = "Leite";
		produtos[idLeite].valor = 0.45;
		produtos[idLeite].quantidade = 10;
		
		produtos[idAcucar] = new Estoque();
		produtos[idAcucar].nomeProduto = "A�ucar";
		produtos[idAcucar].valor = 0.20;
		produtos[idAcucar].quantidade = 20;				
	}
	
	public void imprimirEstoque(){
		System.out.println("Produtos:");
		System.out.println("");
		
    	for (Estoque i: produtos) {
    		i.imprimir();
    		System.out.println("");
    	}
    	
    	System.out.println("");
	}
	
	public void veficarDisponibilidade(int idProduto, int quant) {
		
		System.out.print("Verificando disponibilidade de "+quant+" unidade(s) de ");
		
		switch(idProduto)
		{
			case idCafe:
				System.out.print("caf�: ");
				break;
			case idLeite:
				System.out.print("leite: ");
				break;
			case idAcucar:
				System.out.print("a�ucar: ");
				break;				
		}

		if(produtos[idProduto].verificarDisponibilidade(quant)==1){
			System.out.println("Dispon�vel");
		}
		else {
			System.out.println("Indispon�vel");
		}
		System.out.println("");    	
	}

	
	public void retiraEstoque(int idProduto, int quant) {
		
		System.out.print("Retirando do estoque a quantidade de "+quant+" unidade(s) de ");
		
		switch(idProduto)
		{
			case idCafe:
				System.out.print("caf�: ");
				break;
			case idLeite:
				System.out.print("leite: ");
				break;
			case idAcucar:
				System.out.print("a�ucar: ");
				break;				
		}

		if(produtos[idProduto].removerProdutos(quant)==1){
			System.out.println("Produto retirado.");
		}
		else {
			System.out.println("Produto indispon�vel.");
		}
		System.out.println("");    	
	}

	
	public static void main(String[] args) {
		
		
		PrincipalEstoque Estoque = new PrincipalEstoque();
		
		Estoque.inicializar();
		
		Estoque.imprimirEstoque();
		
		Estoque.veficarDisponibilidade(Estoque.idCafe, 15);
		Estoque.veficarDisponibilidade(Estoque.idLeite, 5);
		Estoque.veficarDisponibilidade(Estoque.idAcucar, 5);
		
		Estoque.retiraEstoque(Estoque.idCafe, 15);
		Estoque.retiraEstoque(Estoque.idCafe, 5);
		Estoque.retiraEstoque(Estoque.idCafe, 5);
		Estoque.retiraEstoque(Estoque.idCafe, 1);
		
		Estoque.retiraEstoque(Estoque.idLeite, 5);
		
		Estoque.retiraEstoque(Estoque.idAcucar, 5);

		Estoque.imprimirEstoque();
	}

}
