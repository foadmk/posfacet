package exercicios;

public class Estoque {

	String nomeProduto;
	double valor;
	int quantidade;

	void imprimir() {
		System.out.println("Nome: " + nomeProduto);
		System.out.printf("Valor: R$%.2f\n", valor);
		System.out.println("Quantidade: " + quantidade);
	}

	int verificarDisponibilidade(int quant) {
		if (quant <= quantidade)
			return 1;
		return 0;
	}

	int removerProdutos(int quant){
		if (verificarDisponibilidade(quant)==1){
			quantidade -= quant;
			return 1;
		}
		return 0;
	}
}
