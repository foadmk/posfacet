package exercicios;

public class Computador {

	String marca;
	String cor;
	String modelo;
	String numeroSerie;
	double pre�o;

	void imprimir() {
		System.out.println("Marca: " + marca);
		System.out.println("Cor: " + cor);
		System.out.println("Modelo: " + modelo);
		System.out.println("N�mero de S�rie: " + numeroSerie);
		System.out.printf("Pre�o: R$%.2f\n", pre�o);
	}

	void calculaValor() {
		switch (marca) {
		case "HP":
			pre�o += (pre�o * 30) / 100;
			break;
		case "IBM":
			pre�o += (pre�o * 50) / 100;
			break;
		}
	}

	int AlterarValor(double valor) {
		if (valor > 0) {
			pre�o = valor;
			return 1;
		}
		return 0;
	}

}
