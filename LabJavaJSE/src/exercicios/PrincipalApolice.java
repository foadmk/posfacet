package exercicios;

public class PrincipalApolice {

	public static void main(String[] args) {

		Apolice novoObj = new Apolice();

		novoObj.nomeSegurado = "Foad Mobini Kesheh";
		novoObj.idade = 29;
		novoObj.valorPremio = 100.00;

		novoObj.imprimir();

		novoObj.calcularPremioApolice();

		novoObj.imprimir();

		novoObj.oferecerDesconto("Curitiba");

		novoObj.imprimir();

	}

}
