package exercicios;

public class Acampamento {

	String nome;
	String equipe;
	int idade;

	void imprimir() {
		System.out.println("Nome: " + nome);
		System.out.println("Equipe: " + equipe);
		System.out.println("Idade: " + idade);
	}

	void separarGrupo() {
		if ((idade >= 6) && (idade <= 10)) {
			equipe = "A";
		} else if ((idade >= 11) && (idade <= 20)) {
			equipe = "B";
		} else if (idade >= 21) {
			equipe = "C";
		}
	}

}
