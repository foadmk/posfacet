package exercicios;

public class Apolice {

	String nomeSegurado;
	int idade;
	double valorPremio;

	void imprimir() {
		System.out.println("Nome: " + nomeSegurado);
		System.out.println("Idade: " + idade);
		System.out.printf("Pr�mio: R$%.2f\n", valorPremio);
	}

	void calcularPremioApolice() {

		if ((idade >= 18) && (idade <= 25)) {
			valorPremio += (valorPremio * 20) / 100;
		} else if ((idade > 25) && (idade <= 36)) {
			valorPremio += (valorPremio * 15) / 100;
		} else if (idade > 36) {
			valorPremio += (valorPremio * 10) / 100;
		}

	}

	void oferecerDesconto(String cidade) {

		double descontoPercentual = 0;

		switch (cidade) {
		case "Curitiba":
			descontoPercentual = 20;
			break;
		case "Rio de Janeiro":
			descontoPercentual = 15;
			break;
		case "S�o Paulo":
			descontoPercentual = 10;
			break;
		case "Belo Horizonte":
			descontoPercentual = 5;
			break;
		}

		valorPremio *= ((100.0 - descontoPercentual) / 100.0);
	}

}
