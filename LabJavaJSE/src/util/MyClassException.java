package util;

public class MyClassException extends Exception {
	private String mensagem = " ";
	private String classe   = " ";
	private String pacote   = " ";
	
	public MyClassException() {
		super();
	}	
	public MyClassException (String p_str){
		super(p_str);
	}
	public MyClassException (Throwable arg0){
		super(arg0);
	}
	public MyClassException (String p_str, Throwable arg0){
		super(p_str, arg0);
	}
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getPacote() {
		return pacote;
	}
	public void setPacote(String pacote) {
		this.pacote = pacote;
	}


}
