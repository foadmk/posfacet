package modulo07;

public interface ExemploInterfaceDVD {
	public final static double PROMOCAODIA = 15; // 15%

	public void acrescentarLista(ExemploEstruturaDVD obj);

	public void imprimirLista();
}
