package modulo07;

public class MatrizBidimensional extends VetorUnidimensional implements
		InterfaceDimensao {
	private int dim2[][] = null;
	private int coluna;

	
	
	public int[][] getDim2() {
		return dim2;
	}

	public void setDim2(int[][] dim2) {
		this.dim2 = dim2;
	}

	public MatrizBidimensional() {
		this.dim2 = new int[InterfaceDimensao.TAMANHO][InterfaceDimensao.TAMANHO];
	}

	public MatrizBidimensional(int linha, int coluna) {
		if ((linha <= 0) || (linha >= 2000000)) {
			System.out.println("Quantidade de linhas inv�lida.");
		} else if ((coluna <= 0) || (coluna >= 2000000)) {
			System.out.println("Quantidade de colunas inv�lida.");
		} else {
			this.dim2 = new int[linha][coluna];
		}
	}

	public void adicionar(int valor) {
		this.dim2[this.linha][this.coluna] = valor;
		if (this.linha < this.dim2.length) {
			this.coluna++;
		}
		// utiliza-se [linha] para identificar a quantidade de colunas de cada
		// linha
		if (this.coluna == this.dim2[linha].length) {
			this.linha++;
			this.coluna = 0;
		}
		if (this.linha == this.dim2.length) {
			this.linha = 0;
			this.coluna = 0;
			System.out.println("Matriz foi excedida. Recome�ando");
		}
	}

	public void imprimir() {
		for (int i = 0; i < this.dim2.length; i++) { // i contador de linhas
			for (int j = 0; j < this.dim2[linha].length; j++) { // j contador de
																// colunas
				System.out.println("Elemento " + i + " : " + this.dim2[i][j]);
			}
		}
	}
}
