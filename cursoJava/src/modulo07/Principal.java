package modulo07;

import java.util.Scanner;


public class Principal {
	private InterfaceDimensao dimensao = null;

	public static void main(String[] args) {
		Principal obj = new Principal();
		obj.executar();
	}

	private void executar() {
		int opcao = 0;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("**************************************");
			System.out.println("1 - Vetor");
			System.out.println("2 - Matriz");
			System.out.println("3 - Sair");
			System.out.println("Entre com uma op��o: ");
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				// vetorMenu();
				break;
			case 2:
				matrizMenu();
				break;
			default:
				if (opcao == 3) {
					System.exit(0);
				}
				System.out.println("Op��o inv�lida.");
			}
		}
	}

	private void matrizMenu() {
		int opcao = 0;
		Scanner sc = new Scanner(System.in);
		boolean loop = true;
		while (loop) {
			System.out.println("**************************************");
			System.out.println("1 - Adicionar Matriz Tamanho Padr�o.");
			System.out
					.println("2 - Adicionar Matriz Tamanho Especificado via teclado.");
			System.out.println("3 - Imprimir Matriz criada.");
			System.out.println("4 - Sair.");
			System.out.println("Entre com uma op��o: ");
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				matrizAdicionarPadrao();
				break;
			case 2:
				matrizAdicionarTamEspecifico();
				break;
			case 3:
				imprimir();
				break;
			case 4:
				loop = false;
				break;
			}
		}

	}

	private void imprimir() {
		// vai executar o construtor sem par�metros
		this.dimensao.imprimir();
	}

	private void matrizAdicionarPadrao() {
		// vai executar o construtor sem par�metros
		this.dimensao = new MatrizBidimensional();
		// adicionar();
	}

	private void matrizAdicionarTamEspecifico() {
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("**************************************");
			System.out.println("Entre com a quantidade de linhas: ");
			int tamLinha = sc.nextInt();
			System.out.println("Entre com a quantidade de colunas: ");
			int tamColuna = sc.nextInt();
			// vai executar o construtor com dois par�metros do tipo inteiro
			this.dimensao = new MatrizBidimensional(tamLinha, tamColuna);
		} while (((MatrizBidimensional) this.dimensao).getDim2() == null);
		// adicionar();
	}
}
