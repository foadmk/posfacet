package modulo07;

public class PrincipalPolimorfismo {
	final static int TAM_VETOR = 4;

	public void executarVetor(Animal[] p_objAnimal) {
		for (int i = 0; i < TAM_VETOR; i++) {
			p_objAnimal[i].emitirSom();
		}
	}

	public static void main(String[] args) {
		PrincipalPolimorfismo pObj = new PrincipalPolimorfismo();
		Animal objAnimal[] = new Animal[TAM_VETOR];
		objAnimal[0] = new Animal();
		objAnimal[1] = new Felinos();
		objAnimal[2] = new Caninos();
		objAnimal[3] = new Passaros();
		pObj.executarVetor(objAnimal);
	}
}
