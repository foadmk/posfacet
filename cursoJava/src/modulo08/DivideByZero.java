package modulo08;

public class DivideByZero {
	public static void main(String args[]) {
		DivideByZero obj = new DivideByZero();
		try {
			obj.calcular();
		} catch (MyClassException e) {
			System.out.println("Classe: " + e.getClasse());
			System.out.println("Mensagem: " + e.getMessage());
			//e.printStackTrace();
		}
	}

	public void calcular() throws MyClassException {
		try {
			int num = 0;
			num = num / 0; // Aqui o erro ser� lan�ado.
		} catch (ArithmeticException e) {
			MyClassException myObj = new MyClassException(
					"Erro causado pela divis�o por zero");
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem(e.getMessage());
			// Se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		}
	}
}
