package modulo08;

public class ExemploTryCatchSeletivo {
	public static void main(String args[]) {
		try {
			int j = Integer.parseInt(args[0]);
			while (j >= 0) {
				System.out.println(j);
				j--;
			}
		} catch (ArrayIndexOutOfBoundsException e1) {
			System.out.println(" Nao foi fornecido um argumento.");
		} catch (NumberFormatException e2) {
			System.out.println(" Argumento nao e um inteiro.");
		}
	}
}
