package modulo03;

import java.util.Date;

public class Pessoa {

	private String cpf;
	private String nome;
	private String dataNasc;
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}

	Pessoa(){
		
	}
	
	Pessoa(String nome, String cpf, String dataNasc){
		this.nome = nome;
		this.cpf = cpf;
		this.dataNasc = dataNasc;
	}

}
