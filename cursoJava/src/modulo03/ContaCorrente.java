package modulo03;

public class ContaCorrente {
	int conta;
	int agencia;
	double saldo;
	double cpmf;
	String nome;

	public ContaCorrente() {
		this(0, 0, 0.0, 0.0, " ");
	}

	public ContaCorrente(int conta, int agencia, double saldo, double cpmf,
			String nome) {
		super();
		this.conta = conta;
		this.agencia = agencia;
		this.saldo = saldo;
		this.cpmf = cpmf;
		this.nome = nome;
	}
}