package modulo03;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class PrincipalPessoa {

	Pessoa varPessoa;

	public static boolean validaData(String dataString) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);

		try {
			sdf.parse(dataString);
			return true;
		} catch (ParseException e) {
			return false;
		}

	}

	public void execCadastramento() {

		String nome;
		String cpf;
		String dataNasc;

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		System.out.print("Digite o nome                   : ");
		nome = s.next();
		System.out.print("Digite o CPF                    : ");

		cpf = s.next();

		do {
			System.out.print("Digite o Data Nasc. (DD/MM/AAAA): ");
			dataNasc = s.next();
		} while (!validaData(dataNasc));

		varPessoa = new Pessoa(nome, cpf, dataNasc);

	}

	public void execImpressao() {

		System.out.println("Nome      : " + varPessoa.getNome());
		System.out.println("CPF       : " + varPessoa.getCpf());
		System.out.println("Nascimento: " + varPessoa.getDataNasc());

	}

	public static void main(String[] args) {

		PrincipalPessoa menu = new PrincipalPessoa();

		boolean run = true;

		Scanner s = new Scanner(System.in).useDelimiter("\r\n");

		while (run) {
			System.out.println("Principal Pessoa");
			System.out.println(".");
			System.out.println("Digite 1 para Cadastro");
			System.out.println("Digite 2 para Imprimir");
			System.out.println("Digite 3 para Sair");
			System.out.println(".");
			System.out.print("Digite sua op��o: ");
			int opcao = s.nextInt();
			System.out.println(".");

			switch (opcao) {
			case 1:
				menu.execCadastramento();
				break;
			case 2:
				menu.execImpressao();
				break;
			case 3:
				run = false;
				break;
			default:
				System.out.println("Op��o Inv�lida!");
				System.out.println(".");
			}

		}

	}

}
