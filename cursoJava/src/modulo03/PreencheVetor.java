package modulo03;

import java.util.Arrays;

public class PreencheVetor {
	public static void main(String[] args) {
		int vetor[] = new int[5];
		Arrays.fill(vetor, 22);
		for (int i = 0; i < vetor.length; i++) {
			System.out.println(" " + vetor[i]);
		}
	}
}
