package modulo05;

public class ProbHerancaClasseFilha extends ProbHerancaClassePai {
	public ProbHerancaClasseFilha() {
		super();
		metodo();
	}

	public void metodo() {
		System.out.println("Metodo da classe Filha");
	}
}