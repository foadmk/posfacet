package modulo05;

public class PessoaJuridica extends Pessoa {

	private String cnpj;
	private String razaoSocial;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public void imprimir() {
		System.out.println("Pessoa Juridica");
		System.out.println("Nome        : " + getNome());
		System.out.println("Endere�o    : " + getEndereco());
		System.out.println("CNPJ        : " + getCnpj());
		System.out.println("Razao Social: " + getRazaoSocial());
	}
}