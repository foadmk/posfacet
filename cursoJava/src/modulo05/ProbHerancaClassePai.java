package modulo05;

public class ProbHerancaClassePai {
	public ProbHerancaClassePai() {
		super();
		((ProbHerancaClassePai) this).metodo();
	}

	public void metodo() {
		System.out.println("Metodo da classe Pai");
	}
}
