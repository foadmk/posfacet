package modulo05;

public class PessoaPrincipal {

	public static void main(String[] args) {
		PessoaFisica exPF = new PessoaFisica();
		PessoaJuridica exPJ = new PessoaJuridica();

		exPF.setNome("Foad");
		exPF.setEndereco("Rua Joao, 23 - Curitiba - PR");
		exPF.setCpf("045.656.454-34");
		exPF.setRg("5.848.844/SC");

		exPJ.setNome("Foad S.A.");
		exPJ.setEndereco("Rua Joao, 23 - Curitiba - PR");
		exPJ.setCnpj("434.3535.232/0001-01");
		exPJ.setRazaoSocial("Foad CNPJ");

		exPF.imprimir();
		exPJ.imprimir();

		Pessoa exP1;
		Pessoa exP2;

		exP1 = new PessoaFisica();
		exP1.setNome("Foad 2");
		exP1.setEndereco("Rua Joao, 23 - Curitiba - PR");
		((PessoaFisica) exP1).setCpf("045.656.454-34");
		((PessoaFisica) exP1).setRg("5.848.844/SC");
		exP1.imprimir();

		exP2 = new PessoaJuridica();
		exP2.setNome("Foad S.A. 2");
		exP2.setEndereco("Rua Joao, 23 - Curitiba - PR");
		((PessoaJuridica) exP2).setCnpj("434.3535.232/0001-01");
		((PessoaJuridica) exP2).setRazaoSocial("Foad CNPJ");
		exP2.imprimir();

	}
}
