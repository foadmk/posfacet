package modulo05;

public class PessoaFisica extends Pessoa {

	private String cpf;
	private String rg;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public void imprimir() {
		System.out.println("Pessoa Fisica");
		System.out.println("Nome    : " + getNome());
		System.out.println("Endere�o: " + getEndereco());
		System.out.println("CPF     : " + getCpf());
		System.out.println("RG      : " + getRg());
	}
}
