package modulo04;

public class ExemploStringBuilderComparacao {
	public static void main(String[] args) {
		StringBuffer strBuffer = new StringBuffer("");
		StringBuilder strBuilder = new StringBuilder("");
		String str = new String();
		long i;
		long f;;
		
		i = System.currentTimeMillis();
		for (int q = 0; q < 10000000; q++) {
			strBuffer.append("a");
		}
		f = System.currentTimeMillis();
		System.out.println("Tempo StringBuffer em milissegundos: " + (f - i));

		i = System.currentTimeMillis();
		for (int q = 0; q < 10000000; q++) {
			strBuilder.append("a");
		}
		f = System.currentTimeMillis();
		System.out.println("Tempo StringBuilder em milissegundos: " + (f - i));

		i = System.currentTimeMillis();
		for (int q = 0; q < 100000; q++) { // 100x menos que nos outros loop
			str += "a";
		}
		f = System.currentTimeMillis();
		System.out.println("Tempo String em milissegundos: " + ((f - i) * 100)); // tempo x 100
	}
}
