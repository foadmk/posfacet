package modulo04;

import java.util.Vector;

public class TesteVector {
	public static void main(String[] args) {
		Vector vet = new Vector();
		vet.add(10); // Apresenta erro vers�o 1.4 ou menor
		vet.add(20); // Apresenta erro vers�o 1.4 ou menor
		vet.add("x"); // Apresenta erro vers�o 1.4 ou menor
		
		vet.add(new Integer(10));
		vet.add(new Integer(20));
		vet.add(new String("X"));

		System.out.println(vet.get(0));
		System.out.println(vet.get(1));
		System.out.println(vet.get(2));
		
		System.out.println(vet.size());
	}
}
