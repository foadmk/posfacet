package modulo04;

import java.util.Scanner;
import java.util.Vector;

public class ExemploCarroPrincipal {
	
	private Vector<ExemploCarro> carros = new Vector<ExemploCarro>();

	
	private int chassi;
	private String marca;
	private String fabricante;
	private String dtFabricacao;
	
	private void cadastrar() {
		Scanner sc = new Scanner(System.in);
		ExemploCarro obj = new ExemploCarro();
		
		System.out.print("Chassi: ");
		int chassi = sc.nextInt();
		obj.setChassi(chassi);
		
		System.out.print("Marca: ");
		String marca = sc.next();
		obj.setMarca(marca);

		System.out.print("Fabricante: ");
		String fabricante = sc.next();
		obj.setFabricante(fabricante);

		System.out.print("Data de Fabrica��o: ");
		String dtfabricacao = sc.next();
		obj.setDtFabricacao(dtfabricacao);

		this.carros.add(obj);
	}
	
	private void imprimir() {
		for (ExemploCarro obj : carros) {
			System.out.println("*****************************************");
			System.out.println("Chassi            : " + obj.getChassi());
			System.out.println("Marca             : " + obj.getMarca());
			System.out.println("Fabricante        : " + obj.getFabricante());			
			System.out.println("Data de Fabrica��o: " + obj.getDtFabricacao());
			System.out.println("*****************************************");
		}
	}
	
	
	public static void main(String[] args) {
		ExemploCarroPrincipal obj = new ExemploCarroPrincipal();
		int opcao = 0;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("1 - Cadastrar.");
			System.out.println("2 - Imprimir");
			System.out.println("9 - Sair.");
			System.out.print("Entre com uma op��o: ");
			opcao = sc.nextInt();

			switch (opcao) {
			case 1:
				obj.cadastrar();
				break;
			case 2:
				obj.imprimir();
				break;
			default:
				if (opcao == 9) {
					System.exit(0);
				}
				System.out.println("Op��o inv�lida.");
			}
		}
	}
}
