package modulo01;

public class ExemploVariaveis {
	int atributoNaoEstatico;
	static int atributoEstatico;

	public static void main(String[] args) {
		System.out.println("Objeto 1:");
		ExemploVariaveis obj = new ExemploVariaveis ();
		obj.atributoNaoEstatico = 10;
		obj.atributoEstatico = 20;
		obj.imprimir();
		System.out.println("Objeto 2:");		
		ExemploVariaveis obj1 = new ExemploVariaveis ();
		obj1.atributoNaoEstatico = 160;
		obj1.atributoEstatico = 320;
		obj1.imprimir();
		System.out.println("-------------------");
		System.out.println("Objeto 1:");
		obj.imprimir(); 
	}
  
	public void imprimir (){
		System.out.println("Atributo n�o estatico: " + atributoNaoEstatico);
		System.out.println("Atributo estatico: " + atributoEstatico);
	}
}
