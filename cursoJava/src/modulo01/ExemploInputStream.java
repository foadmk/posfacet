package modulo01;

import java.io.IOException;

public class ExemploInputStream {
	public static void main(String[] args) throws IOException {
		System.out.println("Digite algo: ");
		while (true) {
			// L� do teclado, por�m os valores seguem a tabela ASCII. 0
			// representa o c�digo 48
			int i = System.in.read();
			System.out.print((char) i);
		}
	}
}
