package creator;

import java.util.Scanner;

public class PrincipalMesaCreator {
	public static void main(String[] args) {
		double larguraTampa = 0.0;
		double alturaTampa = 0.0;
		double volumeTampa = 0.0;
		double larguraPerna = 0.0;
		double alturaPerna = 0.0;
		double volumePerna = 0.0;
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Digite o valor da Largura da Tampa: ");
		larguraTampa = sc.nextDouble();
		System.out.println("Digite o valor da Altura da Tampa: ");
		alturaTampa = sc.nextDouble();
		System.out.println("Digite o valor do Volume da Tampa: ");
		volumeTampa = sc.nextDouble();
		System.out.println("Digite o valor da Largura da Perna: ");
		larguraPerna = sc.nextDouble();
		System.out.println("Digite o valor da Altura da Perna: ");
		alturaPerna = sc.nextDouble();
		System.out.println("Digite o valor do Volume da Perna: ");
		volumePerna = sc.nextDouble();
		MesaCreator mesa = new MesaCreator(larguraTampa, alturaTampa,   
				volumeTampa, larguraPerna, alturaPerna, volumePerna);  
		
		//Apenas valores s�o repassados e s�o os objetos em si
		//Criar os objetos e passar os objetos no constructor � menos elegante.
		
		mesa.imprimir();
	}
}
