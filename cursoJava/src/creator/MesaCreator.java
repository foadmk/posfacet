package creator;

import java.util.ArrayList;
import java.util.List;

public class MesaCreator {
	Tampa tampa;
	List<Perna> perna = new ArrayList<Perna>();

	public MesaCreator(double larguraTampa, double alturaTampa,
			double volumeTampa, double larguraPerna, double alturaPerna,
			double volumePerna) {
		tampa = new Tampa(larguraTampa, alturaTampa, volumeTampa);
		for (int i = 0; i < 4; i++) {
			perna.add(new Perna(larguraPerna, alturaPerna, volumePerna));
		}
	}

	public void imprimir() {
		System.out.println("\n\n Dados da Mesa");
		System.out.println("Valor da Largura da Tampa: " + tampa.largura);
		System.out.println("Valor da Altura da Tampa: " + tampa.altura);
		System.out.println("Valor do Volume da Tampa: " + tampa.volume);
		for (int i = 0; i < perna.size(); i++) {
			System.out.println("Valor da Largura da Perna: "
					+ perna.get(i).largura);
			System.out.println("Valor da Altura da Perna: "
					+ perna.get(i).altura);
			System.out.println("Valor do Volume da Perna: "
					+ perna.get(i).volume);
		}
		System.out
				.println("\n____________________________________________________");
	}
}
