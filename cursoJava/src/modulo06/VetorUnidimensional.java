package modulo06;

public class VetorUnidimensional extends ClasseAbstrataDimensao {
	protected int[] dim1;
	protected static int linha;
	
    public VetorUnidimensional() {
		// executando o construtor vazio da superclasse
		super();
		this.dim1 = new int[this.TAMANHO];
	}

	public VetorUnidimensional(int param) {
		// executando o construtor vazio da superclasse
		super();
		if ((param <= 0) || (param > 2000000)) {
			System.out.println("Tamanho para o vetor recebido inv�lido.");
		} else {
			this.dim1 = new int[param];
		}
	}

	@Override
	public void adicionar(int valor) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void imprimir() {
		// TODO Auto-generated method stub
		
	}
}
