package todo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;

public class SQLite {

	static String insereVariavel(String sql, int variavel) {
		sql += Integer.toString(variavel);
		return sql;
	}

	static String insereVariavel(String sql, Date variavel) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		sql += "'" + df.format(variavel) + "'";
		return sql;
	}

	static String insereIgual(String sql, String nome, int variavel) {

		sql += nome + "=";
		sql = insereVariavel(sql, variavel);
		return sql;
	}

	static String insereIgual(String sql, String nome, String variavel) {

		sql += nome + "=";
		sql = insereVariavel(sql, variavel);
		return sql;
	}

	static String insereIgual(String sql, String nome, Date variavel) {

		sql += nome + "=";
		sql = insereVariavel(sql, variavel);
		return sql;
	}

	static String insereVariavel(String sql, String variavel) {
		sql += "'" + variavel + "'";
		return sql;
	}

	static String insereInicio(String sql) {
		sql += "VALUES (";
		return sql;
	}

	static String insereVirgula(String sql) {
		sql += ",";
		return sql;
	}

	static String insereFinal(String sql) {
		sql += ");";
		return sql;
	}

	public static void executeSQL(String db, String sql) {
		Connection c = ConexaoDB(db);

		try {
			//System.out.println("Executing: " + sql);
			Statement stmt = c.createStatement();
			stmt.setQueryTimeout(30); // set timeout to 30 sec.
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		//System.out.println("Executed successfully");
	}

	private static Connection ConexaoDB(String db) {

		Connection c = null;

		try {
			Class.forName("org.sqlite.JDBC");
			//System.out.println("Opennig: " + "jdbc:sqlite:" + db);
			c = DriverManager.getConnection("jdbc:sqlite:" + db);

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return c;

	}

	public static CachedRowSet selectSQL(String db, String sql)
			throws ParseException, SQLException {

		Connection c = ConexaoDB(db);

		//System.out.println("Executing: " + sql);
		CachedRowSet result = new CachedRowSetImpl();
		
		ResultSet rs;

		Statement stmt = c.createStatement();
		rs = stmt.executeQuery(sql);

		result.populate(rs);

		stmt.close();
		c.close();

		//System.out.println("Operation done successfully");
		return result;
	}

	public static void dropTable(String db, String table) {

		executeSQL(db, "DROP TABLE IF EXISTS " + table + ";");
	}

}
