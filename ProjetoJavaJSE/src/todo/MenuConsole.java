package todo;

import java.util.Scanner;
import java.util.Vector;


public class MenuConsole {
	
	public static int exibirMenu(String nomeMenu, Vector<String> opcoes){
		return exibirMenu(nomeMenu, opcoes, false);
	}

	public static int exibirMenu(String nomeMenu, Vector<String> opcoes, boolean com_espacamento){
		if(com_espacamento){
			System.out.println("\n\n\n\n");
		}
		System.out.println(nomeMenu+" ============================================");
		int i;
		for(i=0; i<opcoes.size(); i++){
			int numero = i+1;
			System.out.println(numero+" - "+opcoes.get(i));
		}
		System.out.print("Op��o desejada: ");
		Scanner entrada = new Scanner(System.in);
		int opcao_escolhida = entrada.nextInt();
		if(opcao_escolhida < 0 || opcao_escolhida > opcoes.size()){
			System.out.println("Op��o inv�lida.");
			return 0;
		} else {
			return opcao_escolhida;
		}
	}

}
