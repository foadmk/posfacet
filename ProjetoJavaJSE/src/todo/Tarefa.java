package todo;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

public class Tarefa extends ItemLista {

	public Tarefa(int id) throws ParseException, SQLException {
		super(id);
	}

	public Tarefa(int id, Projeto pai, String nome,
			Date dataCriacao, Date dataConclusao, int prioridade,
			String descricao, String notas, int concluido) throws ParseException, SQLException {
		super();
		super.setId(id);
		super.setNivel(1);
		super.setIdNivelPai(pai.getId());
		super.setNome(nome);
		super.setDataCriacao(dataCriacao);
		super.setDataConclusao(dataConclusao);
		super.setPrioridade(prioridade);
		super.setDescricao(descricao);
		super.setNotas(notas);
		super.setConcluido(concluido);
	}
	
	public Tarefa(int id, Projeto pai, String nome,
			String dataCriacao, String dataConclusao, int prioridade,
			String descricao, String notas, int concluido) throws ParseException, SQLException {
		super();
		super.setId(id);
		super.setNivel(1);
		super.setIdNivelPai(pai.getId());
		super.setNome(nome);
		super.setDataCriacao(dataCriacao);
		super.setDataConclusao(dataConclusao);
		super.setPrioridade(prioridade);
		super.setDescricao(descricao);
		super.setNotas(notas);
		super.setConcluido(concluido);
	}

	
	public Tarefa(int id, Projeto pai, String nome, int prioridade, String descricao,
			 String notas ) throws ParseException, SQLException {

		super();
		super.setId(id);
		super.setNivel(1);
		super.setIdNivelPai(pai.getId());
		super.setNome(nome);
		setDataCriacao(new Date());
		setDataConclusao(new Date());
		super.setPrioridade(prioridade);
		super.setDescricao(descricao);
		super.setNotas(notas);
		super.setConcluido(0);
		
	}

	public Tarefa(int id, int nivel, int idNivelPai, String nome,
			Date dataCriacao, Date dataConclusao, int prioridade,
			String descricao, String notas, int concluido) {
		super(id, nivel, idNivelPai, nome, dataCriacao, dataConclusao, prioridade,
				descricao, notas, concluido);
	}

	public boolean concluido()
	{
		return getConcluido()==1;
	}
	
	public void alterar(String nome, int prioridade, String descricao,
			String notas) throws ParseException, SQLException {
		
		super.setNome(nome);
		super.setPrioridade(prioridade);
		super.setDescricao(descricao);
		super.setNotas(notas);
		gravar();
		
	}

	public void marcarComoConcluido() throws ParseException, SQLException {
		setConcluido(1);	
		setDataConclusao(new Date());
		gravar();

	}

	public void marcarComoNaoConcluido() throws ParseException, SQLException {
		setConcluido(0);
		gravar();

	}

}
