package todo;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;
import java.util.Vector;

public class Principal extends MenuConsole {

	private static boolean jaExibiuAlgo = false;

	/*
	 * Esta fun��o deve buscar os projetos distintos
	 */
	private static Vector<Projeto> buscarProjetos() throws ParseException,
			SQLException {
		return Database.selectProjetos();
	}

	/*
	 * Esta fun��o deve buscar as tarefas com base no termo de busca
	 */
	@SuppressWarnings("unused")
	private static Vector<Tarefa> buscarTarefas(String termo_busca)
			throws ParseException, SQLException {
		return Database.buscaTarefas(termo_busca);
	}

	/*
	 * Esta fun��o deve buscar todas as tarefas do projeto.
	 */
	@SuppressWarnings("unused")
	private static Vector<Tarefa> buscarTarefasProjeto(Projeto proj)
			throws ParseException, SQLException {
		return Database.selectItensProjeto(proj);
	}

	public static void main(String[] args) throws ParseException, SQLException {

		Database.createTable();
		while (true) {
			abrirProjetos();
		}

	}

	private static void abrirProjetos() throws ParseException, SQLException {

		Vector<Projeto> projetosEncontrados;

		projetosEncontrados = buscarProjetos();

		// Montar menu: ===========================================
		Vector<String> opcoes = new Vector<String>();
		if (projetosEncontrados.size() == 0) {
			System.out.println("Nenhum projeto cadastrado.");
		} else {
			for (int i = 0; i < projetosEncontrados.size(); i++) {
				opcoes.add("Projeto: " + projetosEncontrados.get(i).getNome());
			}
		}
		opcoes.add("Busca de tarefas");
		opcoes.add("Tarefas n�o conclu�das");
		opcoes.add("Adicionar projeto");
		opcoes.add("Sair");

		// A��o conforme a a��o escolhida: ========================
		int opcao_escolhida;
		opcao_escolhida = exibirMenu("Projetos", opcoes, jaExibiuAlgo);
		jaExibiuAlgo = true;
		if (opcao_escolhida <= projetosEncontrados.size()) {
			int chave = opcao_escolhida - 1;
			abrirProjeto(projetosEncontrados.get(chave));
		} else if (opcao_escolhida == (projetosEncontrados.size() + 1)) {
			abrirBusca();
		} else if (opcao_escolhida == (projetosEncontrados.size() + 2)) {
			listarTarefasBusca(Database.selecionaNaoConcluido());
		} else if (opcao_escolhida == (projetosEncontrados.size() + 3)) {
			novoProjeto();
		} else {
			sair();
		}

	}

	private static void abrirProjeto(Projeto proj) throws ParseException,
			SQLException {

		// Espa�amento
		System.out.print("\n\n\n\n\n\n");

		listarTarefasProjeto(proj);
	}

	private static void listarTarefasProjeto(Projeto projeto)
			throws ParseException, SQLException {
		// Montar menu: ===========================================

		Vector<Tarefa> tarefasEncontradas = Database
				.selectItensProjeto(projeto);

		Vector<String> opcoes = new Vector<String>();

		if (tarefasEncontradas.size() == 0) {
			System.out.println("Nenhuma tarefa cadastrada.");
		} else {
			for (Tarefa x : tarefasEncontradas) {
				String temp = "";
				if (x.concluido()) {
					temp = " (Conclu�do)";
				}
				opcoes.add("Tarefa: " + x.getNome() + temp);
			}
		}

		opcoes.add("Adicionar tarefa");
		opcoes.add("Alterar projeto");
		opcoes.add("Excluir projeto");
		opcoes.add("Voltar");

		// A��o conforme a a��o escolhida: ========================
		int opcao_escolhida = exibirMenu("Tarefas do " + projeto.getNome(),
				opcoes);
		if (opcao_escolhida <= tarefasEncontradas.size()) {
			int chave = opcao_escolhida - 1;
			abrirTarefa(tarefasEncontradas.get(chave));
		} else if (opcao_escolhida == (tarefasEncontradas.size() + 1)) {
			novoItem(projeto);
		} else if (opcao_escolhida == (tarefasEncontradas.size() + 2)) {
			alterarProjeto(projeto);
		} else if (opcao_escolhida == (tarefasEncontradas.size() + 3)) {
			excluirProjeto(projeto);
		}
	}

	private static void listarTarefasBusca(Vector<Tarefa> tarefas)
			throws ParseException, SQLException {
		// Montar menu: ===========================================
		Vector<String> opcoes = new Vector<String>();

		if (tarefas.size() == 0) {
			System.out.println("Nenhuma tarefa encontrada.");
		} else {
			for (Tarefa tarefa : tarefas) {
				Projeto proj = Database.getProjetoDaTarefa(tarefa);

				String concluido = "";

				if (tarefa.concluido()) {
					concluido = " (Conclu�do)";
				}

				opcoes.add("Projeto: " + proj.getNome() + "\tTarefa: "
						+ tarefa.getNome() + concluido);
			}
		}
		opcoes.add("Voltar");

		// A��o conforme a a��o escolhida: ========================
		int opcao_escolhida;
		opcao_escolhida = exibirMenu("Resultados de busca", opcoes);

		if (opcao_escolhida <= tarefas.size()) {
			int chave = opcao_escolhida - 1;
			Tarefa tarefaEscolhida = tarefas.get(chave);
			abrirTarefa(tarefaEscolhida);
		}
	}

	private static void abrirTarefa(Tarefa tarefa) throws ParseException,
			SQLException {

		Projeto projeto = Database.getProjetoDaTarefa(tarefa);

		System.out
				.println("\n\n\n\n\n\n====================================================\n");
		String status;
		if (tarefa.getConcluido() == 1) {
			status = "conclu�da";
		} else {
			status = "em aberto";
		}
		System.out.println(tarefa.getNome());
		System.out.println("Tarefa " + status + " do projeto "
				+ projeto.getNome());
		System.out.println("Descri��o: " + tarefa.getDescricao());

		System.out.println("Criado em " + tarefa.getDataCriacaoStr());
		if (tarefa.concluido()) {
			System.out.println("Conclu�do em " + tarefa.getDataConclusaoStr());
		}
		String prioridade = "";
		switch (tarefa.getPrioridade()) {
		case 1:
			prioridade = "alta";
			break;
		case 2:
			prioridade = "m�dia";
			break;
		case 3:
			prioridade = "baixa";
			break;
		}
		System.out.println("Prioridade: " + prioridade);
		if (tarefa.getNotas() != null) {
			System.out.println("\n" + tarefa.getNotas());
		}
		System.out.print("\n");

		// Montar menu:
		Vector<String> opcoes = new Vector<String>();
		opcoes.add("Alterar tarefa");
		opcoes.add("Excluir tarefa");
		if (tarefa.getConcluido() == 1) {
			opcoes.add("Marcar como n�o conclu�do");
		} else {
			opcoes.add("Marcar como conclu�do");
		}
		opcoes.add("Voltar");

		// A��o conforme a a��o escolhida: ========================
		int opcao_escolhida = exibirMenu("Op��es", opcoes);
		switch (opcao_escolhida) {
		case 1:
			alterarItem(tarefa);
			break;
		case 2:
			excluirTarefa(tarefa);
			break;
		case 3:
			alterarStatus(tarefa);
		}

	}

	private static void sair() {
		System.out.println("Bom trabalho! ==============================");
		System.exit(0);
	}

	/*
	 * Caso esta fun��o receba um ItemLista no argumento, altera esse tarefa.
	 * Sen�o, cadastra.
	 */

	private static void alterarItem(Tarefa tarefa) throws ParseException,
			SQLException {
		abrirFormularioTarefa(tarefa, Database.getProjetoDaTarefa(tarefa));
	}

	private static void novoItem(Projeto projeto) throws ParseException,
			SQLException {
		abrirFormularioTarefa(null, projeto);
	}

	private static void alterarProjeto(Projeto projeto) throws ParseException,
			SQLException {
		abrirFormularioProjeto(projeto);
	}

	private static void novoProjeto() throws ParseException, SQLException {
		abrirFormularioProjeto(null);
	}

	private static void abrirFormularioProjeto(Projeto projeto)
			throws ParseException, SQLException {
		Scanner entrada = new Scanner(System.in);

		// Pedir dados pro usu�rio:
		if (projeto != null) {
			System.out
					.println("\nAltera��o de projeto =========================");
		} else {
			System.out
					.println("\nNovo projeto =================================");
		}
		System.out.print("Nome: ");
		String nome = entrada.nextLine();

		if (projeto != null) {
			System.out.print("Confirma a altera��o? (s/n): ");
		} else {
			System.out.print("Confirma o cadastro? (s/n): ");
		}
		String confirmacao = entrada.nextLine();

		// Confirmar e alterar
		if (confirmacao.equals("s")) {

			if (projeto != null) {
				projeto.alterar(nome);
			} else {
				Projeto novoItem = new Projeto(Database.nextID(), nome);
				Database.insertTable(novoItem);
			}
		}
	}

	private static void abrirFormularioTarefa(Tarefa tarefa, Projeto projeto)
			throws ParseException, SQLException {
		Scanner entrada = new Scanner(System.in);

		// Pedir dados pro usu�rio:
		if (tarefa != null) {
			System.out
					.println("\nAltera��o de tarefa =========================");
		} else {
			System.out
					.println("\nNova tarefa =================================");
		}
		System.out.print("Nome: ");
		String nome = entrada.nextLine();
		System.out.print("Prioridade (1=alta, 2=m�dia, 3=baixa): ");
		String prioridade = entrada.nextLine();
		System.out.print("Descri��o: ");
		String descricao = entrada.nextLine();
		System.out.print("Notas: ");
		String notas = entrada.nextLine();
		if (tarefa != null) {
			System.out.print("Confirma a altera��o? (s/n): ");
		} else {
			System.out.print("Confirma o cadastro? (s/n): ");
		}
		String confirmacao = entrada.nextLine();

		// Confirmar e alterar
		if (confirmacao.equals("s")) {
			int prior = Integer.parseInt(prioridade);
			if (tarefa != null) {
				tarefa.alterar(nome, prior, descricao, notas);
				abrirTarefa(tarefa); // Reexibir tarefa
			} else {
				Tarefa novoItem = new Tarefa(Database.nextID(), projeto, nome,
						prior, descricao, notas);
				Database.insertTable(novoItem);
				abrirTarefa(novoItem); // Reexibir tarefa
			}
		}
	}

	private static void excluirTarefa(Tarefa tarefa) throws SQLException,
			ParseException {

		Projeto projeto = Database.getProjetoDaTarefa(tarefa);
		// Confirmar e excluir:
		System.out.print("Deseja mesmo excluir esta tarefa? (s/n)");
		Scanner entrada = new Scanner(System.in);
		String confirmacao = entrada.next();
		if (confirmacao.equals("s")) {
			Database.excluirEntrada(tarefa);
		}
		// Voltar pra listagem do projeto:
		abrirProjeto(projeto);
	}

	private static void excluirProjeto(Projeto projeto) throws SQLException,
			ParseException {

		Vector<Tarefa> tarefasDoProjeto = Database.selectItensProjeto(projeto);
		
		// Confirmar e excluir:
		System.out.print("Deseja mesmo excluir este projeto e todas suas tarefas? (s/n)");
		Scanner entrada = new Scanner(System.in);
		String confirmacao = entrada.next();
		if (confirmacao.equals("s")) {
			for( Tarefa x : tarefasDoProjeto)
			Database.excluirEntrada(x);
		}
		Database.excluirEntrada(projeto);
	}

	public static void alterarStatus(Tarefa tarefa) throws ParseException,
			SQLException {
		if (!tarefa.concluido()) {
			tarefa.marcarComoConcluido();
		} else {
			tarefa.marcarComoNaoConcluido();
		}
		abrirTarefa(tarefa);
	}

	public static void abrirBusca() throws ParseException, SQLException {
		// Pegar termo de busca do usu�rio
		System.out
				.println("\n\n\n\n\nBusca Tarefas==========================================");
		Scanner entrada = new Scanner(System.in);
		System.out.print("Termo de busca: ");
		String termo = entrada.nextLine();

		// Listar resultados:
		System.out.print("\n\n\n\n\n\n");
		listarTarefasBusca(Database.buscaTarefas(termo));
	}
}
