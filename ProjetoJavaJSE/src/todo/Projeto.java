package todo;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

public class Projeto extends ItemLista {

	
	public Projeto() {

	}

	public Projeto(int id, String nome) throws ParseException, SQLException {
		super.setId(id);
		super.setNivel(0);
		super.setIdNivelPai(0);
		super.setNome(nome);
		super.setDataCriacao(new Date());
		super.setDataConclusao("31/12/2999");
		super.setPrioridade(0);
		super.setDescricao("");
		super.setNotas("");
		super.setConcluido(0);
	}



	public Projeto(int id, int nivel, int idNivelPai, String nome,
			Date dataCriacao, Date dataConclusao, int prioridade,
			String descricao, String notas, int concluido) {
		super(id, nivel, idNivelPai, nome, dataCriacao, dataConclusao, prioridade,
				descricao, notas, concluido);
	}

	public Projeto(int id) throws ParseException, SQLException {
		super(id);
	}

	public void alterar(String nome) throws ParseException, SQLException {
		super.setNome(nome);	
		super.gravar();
	}
}
