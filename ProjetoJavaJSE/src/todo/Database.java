package todo;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.sql.rowset.CachedRowSet;

public class Database {

	private static final String TABLE = "TAREFA";
	private static final String DB = "tarefas.db";

	private static String insertString(ItemLista item) {

		String sql = "INSERT INTO " + TABLE
				+ " (ID, NIVEL,IDNIVELPAI,NOME,DATACRIACAO,"
				+ "DATACONCLUSAO,PRIORIDADE,DESCRICAO,NOTAS,CONCLUIDO) ";

		sql = SQLite.insereInicio(sql);
		sql = SQLite.insereVariavel(sql, item.getId());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getNivel());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getIdNivelPai());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getNome());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getDataCriacao());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getDataConclusao());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getPrioridade());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getDescricao());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getNotas());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereVariavel(sql, item.getConcluido());
		sql = SQLite.insereFinal(sql);

		return sql;

	}

	private static String updateString(ItemLista item) {

		/*
		 * UPDATE Customers SET ContactName='Alfred Schmidt', City='Hamburg'
		 * WHERE CustomerName='Alfreds Futterkiste';
		 */

		String sql = "UPDATE " + TABLE + " SET ";
		sql = SQLite.insereIgual(sql, "NIVEL", item.getNivel());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "IDNIVELPAI", item.getIdNivelPai());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "NOME", item.getNome());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "DATACRIACAO", item.getDataCriacao());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "DATACONCLUSAO", item.getDataConclusao());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "PRIORIDADE", item.getPrioridade());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "DESCRICAO", item.getDescricao());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "NOTAS", item.getNotas());
		sql = SQLite.insereVirgula(sql);
		sql = SQLite.insereIgual(sql, "CONCLUIDO", item.getConcluido());
		sql += " WHERE ID=" + String.valueOf(item.getId()) + ";";

		return sql;

	}

	private static String createString() {

		String sql = "CREATE TABLE IF NOT EXISTS " + TABLE + " ("
				+ " ID 	INTEGER PRIMARY KEY NOT NULL,"
				+ " NIVEL          INTEGER     NOT NULL, "
				+ " IDNIVELPAI     INTEGER     NOT NULL, "
				+ " NOME           TEXT    NOT NULL, "
				+ " DATACRIACAO    DATE    NOT NULL, "
				+ " DATACONCLUSAO   DATE    NOT NULL, "
				+ " PRIORIDADE     INTEGER     NOT NULL, "
				+ " DESCRICAO   	 TEXT    NOT NULL, "
				+ " NOTAS   		 TEXT    NOT NULL, "
				+ " CONCLUIDO      INTEGER     NOT NULL " + ");";

		return sql;

	}

	public static void insertTable(ItemLista item) {

		SQLite.executeSQL(DB, insertString(item));

	}

	public static void updateTable(ItemLista item) {

		SQLite.executeSQL(DB, updateString(item));

	}

	public static Projeto loadProjeto(Projeto item) throws ParseException,
			SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + " WHERE ID="
				+ String.valueOf(item.getId()));
		Vector<Projeto> results = parseProjetos(rs);

		return results.get(0);
	}

	public static Tarefa loadTarefa(Tarefa item) throws ParseException,
			SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + " WHERE ID="
				+ String.valueOf(item.getId()));
		Vector<Tarefa> results = parseTarefas(rs);

		return results.get(0);
	}

	public static ItemLista loadItemLista(int id) throws ParseException,
			SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + " WHERE ID="
				+ String.valueOf(id));
		Vector<ItemLista> results = parseItens(rs);

		return results.get(0);
	}

	public static Projeto getProjetoDaTarefa(Tarefa item)
			throws ParseException, SQLException {

		return Database.loadProjeto(new Projeto(item.getIdNivelPai()));

	}

	public static void excluirEntrada(ItemLista item) throws SQLException {

		SQLite.executeSQL(
				DB,
				"DELETE FROM " + TABLE + " WHERE ID="
						+ String.valueOf(item.getId()));

	}

	public static boolean existsRecord(int id) throws ParseException,
			SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + " WHERE ID="
				+ String.valueOf(id));
		Vector<ItemLista> results = parseItens(rs);
		return !results.isEmpty();
	}

	public static int nextID() throws ParseException, SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE
				+ " ORDER BY ID DESC");

		Vector<ItemLista> results = parseItens(rs);

		if (results.isEmpty()) {
			return 1;
		} else {
			return (results.get(0).getId() + 1);
		}

	}

	public static Vector<ItemLista> selectAll() throws ParseException,
			SQLException {
		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + ";");
		return parseItens(rs);
	}

	public static Vector<Tarefa> selectItensProjeto(ItemLista projeto)
			throws ParseException, SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB,
				"SELECT * FROM " + TABLE + " WHERE NIVEL=1 AND IDNIVELPAI="
						+ String.valueOf(projeto.getId()));
		return parseTarefas(rs);
	}

	public static Vector<Projeto> selectProjetos() throws ParseException,
			SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + " WHERE NIVEL=0");
		return parseProjetos(rs);
	}

	public static Vector<Tarefa> buscaTarefas(String Busca)
			throws ParseException, SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE
				+ " WHERE NIVEL=1 AND NOME like '%" + Busca +"%'");
		return parseTarefas(rs);
	}

	public static Vector<ItemLista> buscaGeral(String Busca)
			throws ParseException, SQLException {

		CachedRowSet rs = null;
		rs = SQLite.selectSQL(DB, "SELECT * FROM " + TABLE + " WHERE NOME="
				+ Busca);
		return parseItens(rs);
	}

	public static Vector<ItemLista> parseItens(CachedRowSet rs)
			throws SQLException, ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Vector<ItemLista> results = new Vector<ItemLista>();

		while (rs.next()) {
			ItemLista novoItem = new ItemLista(rs.getInt("id"),
					rs.getInt("nivel"), rs.getInt("idnivelpai"),
					rs.getString("nome"),
					df.parse(rs.getString("datacriacao")), df.parse(rs
							.getString("dataconclusao")),
					rs.getInt("prioridade"), rs.getString("descricao"),
					rs.getString("notas"), rs.getInt("concluido"));
			results.add(novoItem);
		}
		return results;
	}

	public static Vector<Tarefa> parseTarefas(CachedRowSet rs)
			throws SQLException, ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Vector<Tarefa> results = new Vector<Tarefa>();

		while (rs.next()) {
			Tarefa novoItem = new Tarefa(rs.getInt("id"), rs.getInt("nivel"),
					rs.getInt("idnivelpai"), rs.getString("nome"), df.parse(rs
							.getString("datacriacao")), df.parse(rs
							.getString("dataconclusao")),
					rs.getInt("prioridade"), rs.getString("descricao"),
					rs.getString("notas"), rs.getInt("concluido"));
			results.add(novoItem);
		}
		return results;
	}

	public static Vector<Projeto> parseProjetos(CachedRowSet rs)
			throws SQLException, ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Vector<Projeto> results = new Vector<Projeto>();

		while (rs.next()) {
			Projeto novoItem = new Projeto(rs.getInt("id"), rs.getInt("nivel"),
					rs.getInt("idnivelpai"), rs.getString("nome"), df.parse(rs
							.getString("datacriacao")), df.parse(rs
							.getString("dataconclusao")),
					rs.getInt("prioridade"), rs.getString("descricao"),
					rs.getString("notas"), rs.getInt("concluido"));
			results.add(novoItem);
		}
		return results;
	}

	public static Vector<Tarefa> selecionaNaoConcluido() throws SQLException,
			ParseException {

		CachedRowSet rs = null;
		rs = SQLite
				.selectSQL(
						DB,
						"SELECT * FROM "
								+ TABLE
								+ " WHERE CONCLUIDO=0 AND NIVEL=1 ORDER BY DATACRIACAO ASC, PRIORIDADE DESC;");

		return parseTarefas(rs);

	}

	public static void createTable() {

		SQLite.executeSQL(DB, createString());

	}

	public static void dropTable() {

		SQLite.dropTable(DB, TABLE);

	}
}
