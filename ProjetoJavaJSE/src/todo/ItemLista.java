package todo;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ItemLista {

	private int id;
	private int nivel;
	private int idNivelPai;
	private String nome;
	private Date dataCriacao;
	private Date dataConclusao;
	private int prioridade;
	private String descricao;
	private String notas;
	private int concluido;

	public ItemLista() {

	}

	public ItemLista(int id, int nivel, int idNivelPai, String nome,
			Date dataCriacao, Date dataConclusao, int prioridade,
			String descricao, String notas, int concluido) {
		super();
		this.id = id;
		this.nivel = nivel;
		this.idNivelPai = idNivelPai;
		this.nome = nome;
		this.dataCriacao = dataCriacao;
		this.dataConclusao = dataConclusao;
		this.prioridade = prioridade;
		this.descricao = descricao;
		this.notas = notas;
		this.concluido = concluido;
	}

	public ItemLista(int id, int nivel, int idNivelPai, String nome,
			String dataCriacaoTexto, String dataConclusaoTexto, int prioridade,
			String descricao, String notas, int concluido)
			throws ParseException {
		super();

		this.id = id;
		this.nivel = nivel;
		this.idNivelPai = idNivelPai;
		this.nome = nome;
		setDataCriacao(dataCriacaoTexto);
		setDataConclusao(dataConclusaoTexto);
		this.prioridade = prioridade;
		this.descricao = descricao;
		this.notas = notas;
		this.concluido = concluido;

	}

	public ItemLista(int id, int nivel, int idNivelPai, String nome,
			int prioridade, String descricao, String notas)
			throws ParseException {
		super();

		this.id = id;
		this.nivel = nivel;
		this.idNivelPai = idNivelPai;
		this.nome = nome;
		setDataCriacao(new Date());
		setDataConclusao(new Date());
		this.prioridade = prioridade;
		this.descricao = descricao;
		this.notas = notas;
		this.concluido = 0;

	}

	public ItemLista(int id) throws ParseException, SQLException {
		this.id = id;
		Database.loadItemLista(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public int getIdNivelPai() {
		return idNivelPai;
	}

	public void setIdNivelPai(int idNivelPai) {
		this.idNivelPai = idNivelPai;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public String getDataCriacaoStr() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		return df.format(dataCriacao);
	}

	public void setDataCriacao(String dataCriacao) throws ParseException {
		Date d = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		d = df.parse(dataCriacao);
		this.dataCriacao = d;
	}

	public void setDataConclusao(String dataConclusao) throws ParseException {
		Date d = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		d = df.parse(dataConclusao);
		this.dataConclusao = d;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public String getDataConclusaoStr() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		return df.format(dataConclusao);
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public int getConcluido() {
		return concluido;
	}

	public void setConcluido(int concluido) {
		this.concluido = concluido;
	}

	public void imprimir() {

		System.out.println(nivel + "\t" + idNivelPai + "\t" + nome + "\t"
				+ getDataCriacaoStr() + "\t" + getDataConclusaoStr() + "\t"
				+ prioridade + "\t" + descricao + "\t" + notas + "\t"
				+ concluido);
	}

	public void gravar() throws ParseException, SQLException {
		if (Database.existsRecord(id)) {
			Database.updateTable(this);
		} else {
			Database.insertTable(this);
		}

	}

}
