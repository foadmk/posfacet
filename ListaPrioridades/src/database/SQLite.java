package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;

import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;

public class SQLite {


	static String toText(String variavel) {
		String sql = "'" + variavel + "'";
		return sql;
	}
	
	static String toText(boolean variavel) {
		
		String sql ="";
		
		if(variavel){
			sql = "TRUE";
		} else {
			sql = "FALSE";
		}
		return sql;
	}
	
	static String toText(int variavel) {
		String sql = Integer.toString(variavel);
		return sql;
	}
	
	static String toText(long variavel) {
		String sql = Long.toString(variavel);
		return sql;
	}

	static String toText(Timestamp variavel) {


		String sql = "'" + variavel + "'";
		
		return sql;
	}


	public static void executeSQL(String db, String sql) {
		Connection c = ConexaoDB(db);

		try {
			System.out.println("Executing: " + sql);
			Statement stmt = c.createStatement();
			stmt.setQueryTimeout(30); // set timeout to 30 sec.
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Executed successfully");
	}

	private static Connection ConexaoDB(String db) {

		Connection c = null;

		try {
			Class.forName("org.sqlite.JDBC");
			System.out.println("Opennig: " + "jdbc:sqlite:" + db);
			c = DriverManager.getConnection("jdbc:sqlite:" + db);

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return c;

	}

	public static CachedRowSet selectSQL(String db, String sql)
			throws ParseException, SQLException {

		Connection c = ConexaoDB(db);

		System.out.println("Executing: " + sql);
		CachedRowSet result = new CachedRowSetImpl();
		
		ResultSet rs;

		Statement stmt = c.createStatement();
		rs = stmt.executeQuery(sql);

		result.populate(rs);

		stmt.close();
		c.close();

		System.out.println("Operation done successfully");
		return result;
	}

	public static void dropTable(String db, String table) {

		executeSQL(db, "DROP TABLE IF EXISTS " + table + ";");
	}

}
