package database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Vector;

import javax.sql.rowset.CachedRowSet;


public class dbEtapa extends BancoDados {
	
	public static final String DB="contratos.db";
	public static final String TABLE="ETAPAS";
	
	public static final int ID=0;
	public static final int CONTRATO=1;
	public static final int ETAPA=2;
	public static final int RESPONSAVEL=3;
	public static final int NOTAS=4;
	public static final int ESTADO=5;
	public static final int DATA_CRIACAO=6;
	public static final int DATA_CONCLUSAO=7;
	public static final int TS=8;
	
	public static final int QUANT=9;
	
	public long index;
	public String contrato;
	public String etapa;
	Timestamp dataCriacao;
	Timestamp dataConclusao;
	Timestamp ts;
	
	


	public dbEtapa(long index, String contrato, String etapa,
			Timestamp dataCriacao, Timestamp dataConclusao, Timestamp ts) {
		super();
		this.index = index;
		this.contrato = contrato;
		this.etapa = etapa;
		this.dataCriacao = dataCriacao;
		this.dataConclusao = dataConclusao;
		this.ts = ts;
	}

	
	public Vector<dbEtapa> getAll() throws ParseException, SQLException {
		
		String query = selectAllString();
		
		CachedRowSet results = queryRecords(query);
		
		return parseCachedRowSet(results);
	}
	
	public Vector<dbEtapa> parseCachedRowSet(CachedRowSet rs)
			throws SQLException, ParseException {

		Vector<dbEtapa> results = new Vector<dbEtapa>();

		while (rs.next()) {
			dbEtapa novoItem = new dbEtapa(	rs.getLong(sqlName(ID)), 
											rs.getString(sqlName(CONTRATO)),
											rs.getString(sqlName(ETAPA)),
											rs.getTimestamp(sqlName(DATA_CRIACAO)),
											rs.getTimestamp(sqlName(DATA_CONCLUSAO)),
											rs.getTimestamp(sqlName(TS)));
			results.add(novoItem);
		}
		return results;
	}
	
	@Override
	public String sqlName(int var){
		switch (var){
		case ID:				return "ID";
		case CONTRATO:			return "CONTRATO";
		case ETAPA:				return "ETAPA";
		case DATA_CRIACAO:		return "DATA_CRIACAO";
		case DATA_CONCLUSAO:	return "DATA_CONCLUSAO";
		case TS:				return "TS";
		}
		return null;
	}
	
	@Override
	public String sqlType(int var){
		switch (var){
		case ID:				return "INTEGER 	PRIMARY KEY     AUTOINCREMENT	NOT NULL";
		case CONTRATO:			return "TEXT 		NOT NULL";
		case ETAPA:				return "INTEGER    	NOT NULL";
		case DATA_CRIACAO:		return "DATE 		NOT NULL";
		case DATA_CONCLUSAO:	return "DATE";
		case TS:				return "DATE UNIQUE NOT NULL";
		}
		return null;
	}
	
	@Override
	public String sqlValue(int var){
		switch (var){
		case ID:				return SQLite.toText(index);
		case CONTRATO:			return SQLite.toText(contrato);
		case ETAPA:				return SQLite.toText(etapa);
		case DATA_CRIACAO:		return SQLite.toText(dataCriacao);
		case DATA_CONCLUSAO:	return SQLite.toText(dataConclusao);
		case TS:				return SQLite.toText(ts);
		}
		return null;
	}

	@Override
	public void setValue(int var, String value){
		switch (var){
		//case ID:				index = value;
		case CONTRATO:			contrato = value;
		case ETAPA:				etapa = value;
		//case DATA_CRIACAO:	dataCriacao = value;
		//case DATA_CONCLUSAO:	dataConclusao = value;
		//case TS:				ts = value;
		}
	}

	@Override
	public void setValue(int var, long value){
		switch (var){
		case ID:				index = value;
		//case CONTRATO:		contrato = value;
		//case ETAPA:			etapa = value;
		//case DATA_CRIACAO:	dataCriacao = value;
		//case DATA_CONCLUSAO:	dataConclusao = value;
		//case TS:				ts = value;
		}
	}

	@Override
	public void setValue(int var, Timestamp value){
		switch (var){
		//case ID:				index = value;
		//case CONTRATO:			contrato = value;
		//case ETAPA:				etapa = value;
		case DATA_CRIACAO:	dataCriacao = value;
		case DATA_CONCLUSAO:	dataConclusao = value;
		case TS:				ts = value;
		}
	}

	@Override
	public void setValue(int var, int value){
		switch (var){
		//case ID:				index = value;
		//case CONTRATO:			contrato = value;
		//case ETAPA:				etapa = value;
		//case DATA_CRIACAO:	dataCriacao = value;
		//case DATA_CONCLUSAO:	dataConclusao = value;
		//case TS:				ts = value;
		}
	}

	
	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}
	
	@Override
	public int getIndexIdentifier() {
		return ID;
	}
	
	@Override
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());		
	}

}
