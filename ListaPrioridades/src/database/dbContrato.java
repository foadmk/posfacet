package database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Vector;

import javax.sql.rowset.CachedRowSet;


public class dbContrato extends BancoDados {
	
	public static final String DB="contratos.db";
	public static final String TABLE="CONTRATOS";
	
	public static final int ID=0;
	public static final int CONTRATO=1;
	public static final int CLIENTE=2;
	public static final int TIPO=3;
	public static final int PRIORIDADE=4;
	public static final int DATA_CRIACAO=5;
	public static final int DATA_CONCLUSAO=6;
	public static final int TS=7;
	
	public static final int QUANT=8;
	
	public long index;
	public String contrato;
	public String cliente;
	public String tipo;
	public String prioridade;
	Timestamp dataCriacao;
	Timestamp dataConclusao;
	Timestamp ts;
	
	


	
	public dbContrato(long index, String contrato, String cliente, String tipo,
			String prioridade, Timestamp dataCriacao, Timestamp dataConclusao,
			Timestamp ts) {
		super();
		this.index = index;
		this.contrato = contrato;
		this.cliente = cliente;
		this.tipo = tipo;
		this.prioridade = prioridade;
		this.dataCriacao = dataCriacao;
		this.dataConclusao = dataConclusao;
		this.ts = ts;
	}

	
	public dbContrato(String contrato, String cliente, String tipo,
			String prioridade, Timestamp dataCriacao, Timestamp dataConclusao) {
		super();
		this.index = -1;
		this.contrato = contrato;
		this.cliente = cliente;
		this.tipo = tipo;
		this.prioridade = prioridade;
		this.dataCriacao = dataCriacao;
		this.dataConclusao = dataConclusao;
		updateTS();
	}

	public Vector<dbContrato> getAll() throws ParseException, SQLException {
		
		String query = selectAllString();
		
		CachedRowSet results = queryRecords(query);
		
		return parseCachedRowSet(results);
	}
	
	public Vector<dbContrato> parseCachedRowSet(CachedRowSet rs)
			throws SQLException, ParseException {

		Vector<dbContrato> results = new Vector<dbContrato>();

		while (rs.next()) {
			dbContrato novoItem = new dbContrato(	rs.getLong(sqlName(ID)), 
											rs.getString(sqlName(CONTRATO)),
											rs.getString(sqlName(CLIENTE)),
											rs.getString(sqlName(TIPO)),
											rs.getString(sqlName(PRIORIDADE)),
											rs.getTimestamp(sqlName(DATA_CRIACAO)),
											rs.getTimestamp(sqlName(DATA_CONCLUSAO)),
											rs.getTimestamp(sqlName(TS)));
			results.add(novoItem);
		}
		return results;
	}
	
	@Override
	public String sqlName(int var){
		switch (var){
		case ID:				return "ID";
		case CONTRATO:			return "CONTRATO";
		case CLIENTE:			return "CLIENTE";
		case TIPO:				return "TIPO";
		case PRIORIDADE:		return "PRIORIDADE";
		case DATA_CRIACAO:		return "DATA_CRIACAO";
		case DATA_CONCLUSAO:	return "DATA_CONCLUSAO";
		case TS:				return "TS";
		}
		return null;
	}
	
	@Override
	public String sqlType(int var){
		switch (var){
		case ID:				return "INTEGER 	PRIMARY KEY     AUTOINCREMENT	NOT NULL";
		case CONTRATO:			return "TEXT 	NOT NULL";
		case CLIENTE:			return "TEXT 	NOT NULL";
		case TIPO:				return "TEXT 	NOT NULL";
		case PRIORIDADE:		return "TEXT    NOT NULL";
		case DATA_CRIACAO:		return "DATE 	NOT NULL";
		case DATA_CONCLUSAO:	return "DATE";
		case TS:				return "DATE 	UNIQUE NOT NULL";
		}
		return null;
	}
	
	@Override
	public String sqlValue(int var){
		switch (var){
		case ID:				return SQLite.toText(index);
		case CONTRATO:			return SQLite.toText(contrato);
		case CLIENTE:			return SQLite.toText(cliente);
		case TIPO:				return SQLite.toText(tipo);
		case PRIORIDADE:		return SQLite.toText(prioridade);
		case DATA_CRIACAO:		return SQLite.toText(dataCriacao);
		case DATA_CONCLUSAO:	return SQLite.toText(dataConclusao);
		case TS:				return SQLite.toText(ts);
		}
		return null;
	}

	
	@Override
	public void setValue(int var, String value){
		switch (var){
		//case ID:				index = value;
		case CONTRATO:			contrato = value;
		case CLIENTE:			cliente = value;
		case TIPO:				tipo = value;
		case PRIORIDADE:		prioridade = value;
		//case DATA_CRIACAO:		dataCriacao = value;
		//case DATA_CONCLUSAO:	dataConclusao = value;
		//case TS:				ts = value;
		}
	}

	@Override
	public void setValue(int var, long value){
		switch (var){
		case ID:				index = value;
		//case CONTRATO:			contrato = value;
		//case CLIENTE:			cliente = value;
		//case TIPO:				tipo = value;
		//case PRIORIDADE:		prioridade = value;
		//case DATA_CRIACAO:		dataCriacao = value;
		//case DATA_CONCLUSAO:	dataConclusao = value;
		//case TS:				ts = value;
		}
	}
	
	@Override
	public void setValue(int var, int value){
		switch (var){
		//case ID:				index = value;
		//case CONTRATO:			contrato = value;
		//case CLIENTE:			cliente = value;
		//case TIPO:				tipo = value;
		//case PRIORIDADE:		prioridade = value;
		//case DATA_CRIACAO:		dataCriacao = value;
		//case DATA_CONCLUSAO:	dataConclusao = value;
		//case TS:				ts = value;
		}
	}


	@Override
	public void setValue(int var, Timestamp value){
		switch (var){
		//case ID:				index = value;
		//case CONTRATO:			contrato = value;
		//case CLIENTE:			cliente = value;
		//case TIPO:				tipo = value;
		//case PRIORIDADE:		prioridade = value;
		case DATA_CRIACAO:		dataCriacao = value;
		case DATA_CONCLUSAO:	dataConclusao = value;
		case TS:				ts = value;
		}
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}
	
	@Override
	public int getIndexIdentifier() {
		return ID;
	}
	
	@Override
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());		
	}

}
