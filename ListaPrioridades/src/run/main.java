package run;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import database.SQLite;
import database.dbContrato;

public class main {

	static int uiState;
	
	public static void main(String[] args) throws ParseException {

		uiState = 0;
		
		while(uiState != -1){
			
			switch(uiState){
			
				case 0:
					showMenuPrincipal();
					break;

				case 1:
					showAdicCircuito();
					break;
			}
			
		}
			
		
	}
	
	public static void showMenuPrincipal(){

		Scanner input = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println("--------------------------------");
		System.out.println("Menu Principal");
		System.out.println();
		System.out.println("1 - Adicionar novo circuito");
		System.out.println("2 - Procurar circuito");
		System.out.println("9 - Sair");
		System.out.println();
		System.out.print("Selecione uma op��o: ");
		
		int number = input.nextInt();
		
		switch(number){
			case 1:
				uiState = 1;
				break;
			case 2:
				uiState = 2;
				break;
			case 9:
				uiState = -1;
				break;
		}
	}
	
	public static String parsePrioridade(int prioridade){
		
		
		switch(prioridade){
			case 1: return "Baixa";
			case 2: return "M�dia";
			case 3: return "Alta";
		}
		return "NDA";

	}

	public static String parseTipo(int tipo){
				
		switch(tipo){
			case 1: return "Convencional";
			case 2: return "Convencional GPON";
			case 3: return "BEL";
			case 4: return "Ativa��o";
			case 5: return "Remanejamento";
			case 6: return "Fibra Escura";
			case 7: return "Desativa��o";
		}
		return "NDA";

	}


	public static void showAdicCircuito() throws ParseException{

		Scanner input = new Scanner(System.in).useDelimiter("\r\n");

		System.out.println("--------------------------------");
		System.out.println("Adicionar Circuito");
		System.out.println();

		System.out.print("Numero: "); 
		int numero = input.nextInt();

		System.out.print("Cliente: "); 
		String cliente = input.next();

		System.out.print("Tipo (1-Convencional 2-Conv. GPON 3-BEL 4-Ativacao 5-Remanejamento 6-Fibra Escura 7-Desativa��o): "); 
		int tipo = input.nextInt();

		System.out.print("Prioridade (1-Baixa 2-M�dia 3-Alta): "); 
		int prioridade = input.nextInt();

		System.out.print("Data Inicio (dd-mm-aaaa): "); 
		String strDataInicio = input.next();

		System.out.print("Data para Entrega (dd-mm-aaaa): "); 
		String strDataConclusao = input.next();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		
		Timestamp dataInicio = new Timestamp(df.parse(strDataInicio).getTime());
		Timestamp dataConclusao = new Timestamp(df.parse(strDataConclusao).getTime());

		System.out.println();
		System.out.println("Circuito Numero: " + numero);
		System.out.println("Cliente: " + cliente);
		System.out.println("Tipo: " + parseTipo(tipo));
		System.out.println("Prioridade: " + parsePrioridade(prioridade));
		System.out.println("Data Inicio: " + dataInicio);
		System.out.println("Data Conclusao: " + dataConclusao);
		System.out.println();
		System.out.print("Adicionar (s/n): ");
		
		char confirma = input.next().charAt(0);
		
		if(confirma == 's' || confirma =='S'){
			dbContrato novo = new dbContrato(Integer.toString(numero), cliente, parseTipo(tipo), parsePrioridade(prioridade), dataInicio, dataConclusao);
		    novo.newRecord();
		}
		
		uiState = 0;
	}

}
