package visio;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mxgraph.view.mxGraph;

public class Cabo {
	private long id;
	private String nome;
	private String trecho;
	private String tipo;
	private int comprimento;
	private Ponta pontaA, pontaB;

	public Ponta getPontaA() {
		return pontaA;
	}

	public void setPontaA(Ponta pontaA) {
		this.pontaA = pontaA;
	}

	public Ponta getPontaB() {
		return pontaB;
	}

	public void setPontaB(Ponta pontaB) {
		this.pontaB = pontaB;
	}

	public Cabo() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTrecho() {
		return trecho;
	}

	public void setTrecho(String trecho) {
		this.trecho = trecho;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getComprimento() {
		return comprimento;
	}

	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}

}
