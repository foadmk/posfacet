package visio;

import java.util.ArrayList;
import java.util.List;

public class Mapa {

	private List<Quadrante> Quads;

	static final double ang[]={0,90,180,270,45,135,225,315,22.5,67.5,112.5,157.5,202.5,247.5,292.5,337.5};
	//static final double ang[] = { 12, 33, 57, 78, 102, 123, 147, 168, 192, 213, 237, 258, 282, 303, 327, 348 };

	static final int tamX = 10;
	static final int tamY = 10;

	Mapa() {
		Quads = new ArrayList<Quadrante>();
		for (int x = 0; x < tamX; x++) {
			for (int y = 0; y < tamY; y++) {
				Quadrante xy = new Quadrante(x, y);
				Quads.add(xy);
			}
		}
	}

	int ocuparSubQuadrante(int x, int y) {
		int quad = x * tamY + y;
		int ocup = -1;
		Quadrante sel = Quads.get(quad);
		ocup = sel.ocuparSubQuadrante();
		Quads.set(quad, sel);
		return ocup;
	}

	int ocuparSubQuadrante(int x, int y, double angulo) {
		int quad = x * tamY + y;
		int ocup = -1;
		Quadrante sel = Quads.get(quad);

		int i = 0;
		for (double a : ang) {
			if (((angulo - 10.0) < a) && (a < (angulo + 10.0))) {
				sel.ocuparSubQuadrante(i);
			}
			i++;
		}

		Quads.set(quad, sel);
		return ocup;
	}

	static final int cx = 135;
	static final int cy = 135;
	static final int l = 110;

	public int coordX(int x) {
		return cx + (l * 2 + 50) * (x + 0);
	}

	public int coordY(int y) {
		return cy + (l * 2 + 50) * (y + 0);
	}

	public int subCoordX(int x, int pos) {
		return (int) (cx + (l * 2 + 50) * (x + 0) + l
				* (Math.cos(ang[pos] * Math.PI / 180)));
	}

	public int subCoordY(int y, int pos) {
		return (int) (cy + (l * 2 + 50) * (y + 0) + l
				* (Math.sin(ang[pos] * Math.PI / 180)));
	}

}
