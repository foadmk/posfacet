package visio;

import java.util.ArrayList;
import java.util.List;

public class Quadrante {
	
	int x;
	int y;

	List<SubQuadrante> SubQuads;
	
	Quadrante(int x, int y){
		this.x = x;
		this.y = y;
		SubQuads = new ArrayList<SubQuadrante>();
		for(int i=0; i<16; i++){
			SubQuadrante subquad = new SubQuadrante(i, false);
			SubQuads.add(subquad);
		}
	}
	
	public int ocuparSubQuadrante(){
		for(int i=0; i<SubQuads.size(); i++){
			if(!SubQuads.get(i).ocupado){
				SubQuadrante subQuad = SubQuads.get(i);
				subQuad.ocupado = true;
				SubQuads.set(i, subQuad);
				return i;
			}
		}
		return -1;
	}

	public void ocuparSubQuadrante(int i) {
			SubQuadrante subQuad = SubQuads.get(i);
			subQuad.ocupado = true;
			SubQuads.set(i, subQuad);
	}

}
