package visio;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

public class Visio extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2707712944901661771L;

	@SuppressWarnings("unused")
	public Visio() throws ClassNotFoundException, SQLException {
		super("Hello, World!");

		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();

		List<Cabo> listaCabo = null;
		List<Ponta> listaPonta = new ArrayList<Ponta>();

		List<PontaObj> listaPontaObj = new ArrayList<PontaObj>();
		List<CaboObj> listaCaboObj = new ArrayList<CaboObj>();
		
		try {
			listaCabo = CaboDB.getListCabos();
			for (Cabo c : listaCabo) {
				if (!(listaPonta.contains(c.getPontaA()))) {
					listaPonta.add(c.getPontaA());
				}
				if (!(listaPonta.contains(c.getPontaB()))) {
					listaPonta.add(c.getPontaB());
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		graph.getModel().beginUpdate();
		try {
			int cxi = 100;
			int cyi = 100;
			int l = 110;

			// int
			// angles[]={15,30,60,75,105,120,150,165,195,210,240,255,285,300,330,345};
			// int
			// angles[]={0,15,30,45,60,75,90,105,120,135,150,165,180,195,210,225,240,255,270,285,300,315,330,345};
			// double
			// angles[]={0,22.5,45,67.5,90,112.5,135,157.5,180,202.5,225,247.5,270,292.5,315,337.5};

			Mapa mapa = new Mapa();

			// Arruma pontas no Quadrantes Corretos

			int x = 0;
			int y = 1;

			for (Ponta p : listaPonta) {
				
				PontaObj Obj = new PontaObj();
				
				Obj.setPonta(p);
				
				if ((p.getTipoPonta() != 4)) {
					Obj.setQuadX(x);
					Obj.setQuadY(y);

					x++;
				}
				
				listaPontaObj.add(Obj);
			}

			for (Cabo c : listaCabo) {

				PontaObj A = listaPontaObj.get(listaPonta.indexOf(c.getPontaA()));
				PontaObj B = listaPontaObj.get(listaPonta.indexOf(c.getPontaB()));
				
				CaboObj Obj = new CaboObj();
				
				Obj.setPontaA(A);
				Obj.setPontaB(B);

				if (A.getPonta().getTipoPonta() == 4) {
					A.setQuadX(B.getQuadX());
					A.setQuadY(B.getQuadY());
				}

				if (B.getPonta().getTipoPonta() == 4) {
					B.setQuadX(A.getQuadX());
					B.setQuadY(A.getQuadY());
				}
				
				listaCaboObj.add(Obj);
			}

			// Ocupa SubQuandrantes do cabos

			for (CaboObj c : listaCaboObj) {

				PontaObj A = c.getPontaA();
				PontaObj B = c.getPontaB();

				int Ax = mapa.coordX(A.getQuadX());
				int Ay = mapa.coordY(A.getQuadY());
				int Bx = mapa.coordX(B.getQuadX());
				int By = mapa.coordY(B.getQuadY());

				// Analise em rela��o a ponta A
				double angulo = Math.atan2(By - Ay, Bx - Ax) * 180.0 / Math.PI;
				if (angulo < 0)
					angulo += 360.0;
				mapa.ocuparSubQuadrante(A.getQuadX(), A.getQuadY(), angulo);

				// Analise em rela��o a ponta B
				angulo = Math.atan2(Ay - By, Ax - Bx) * 180.0 / Math.PI;
				if (angulo < 0)
					angulo += 360.0;
				mapa.ocuparSubQuadrante(B.getQuadX(), B.getQuadY(), angulo);

			}

			PontaObj.config(graph, parent, mapa);
			CaboObj.config(graph, parent);

			for (PontaObj p : listaPontaObj) {
				p.draw();
			}

			for (CaboObj c : listaCaboObj) {
				c.draw();
			}

			/*
			 * int coordx[] =
			 * {0,1,2,3,0,1,2,2,2,2,2,2,2,2,0,1,0,1,1,4,5,6,6,7,8}; int coordy[]
			 * = {3,3,3,3,2,2,2,4,5,6,7,8,0,1,5,5,6,6,7,0,0,0,1,1,1};
			 * 
			 * int cx = cxi + (l*2+50) * coordx[0]; int cy = cyi + (l*2+50) *
			 * coordy[0];
			 * 
			 * Object pop = graph.insertVertex(parent, null, "ADRSE", cx - 40,
			 * cy - 40, 80, 80, "shape=rectangle");
			 * 
			 * for (int i = 1; i < 25; i++) { cx = cxi + (l*2+50) * coordx[i];
			 * cy = cyi + (l*2+50) * coordy[i];
			 * 
			 * Object ceo1 = graph.insertVertex(parent, null, i, cx - 40, cy -
			 * 40, 80, 80, "shape=triangle"); int j=0; for (double x : angles) {
			 * Object v1 = graph .insertVertex( parent, null,
			 * listaPonta.get(0).getNome() , cx + l (Math.cos(x * Math.PI /
			 * 180)) - 20, cy + (l * Math.sin(x * Math.PI / 180)) - 20, 40, 40,
			 * "shape=ellipse"); j++; Object e1 = graph.insertEdge(parent, null,
			 * null, ceo1, v1); } }
			 */

		} finally {
			graph.getModel().endUpdate();
		}

		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		getContentPane().add(graphComponent);
	}

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Visio frame = new Visio();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(2400, 2700);
		frame.setVisible(true);
	}

}
