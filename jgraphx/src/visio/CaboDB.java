package visio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CaboDB {
	private static Connection ds;

	// connect to DB and get customer list
	public static List<Cabo> getListCabos() throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("select cabo.cabo_id, cabo_nome.nome, cabo.trecho, cabo_tipo.nome, cabo.quant_fibras, cabo.comprimento, cabo.ponta_id_a, cabo.ponta_id_b "
						+ "from cabo, cabo_nome, cabo_tipo "
						+ "where cabo.cabo_nome_id=cabo_nome.cabo_nome_id and cabo.cabo_tipo_id=cabo_tipo.cabo_tipo_id ");

		// get customer data from database
		ResultSet result = ps.executeQuery();

		List<Cabo> list = new ArrayList<Cabo>();

		while (result.next()) {
			Cabo cabo = new Cabo();

			cabo.setId(result.getLong("cabo.cabo_id"));
			cabo.setNome(result.getString("cabo_nome.nome"));
			cabo.setTrecho(result.getString("cabo.trecho"));
			cabo.setTipo(result.getString("cabo_tipo.nome") + " ("
					+ result.getString("cabo.quant_fibras") + ")");
			cabo.setComprimento(result.getInt("cabo.comprimento"));
			cabo.setPontaA(PontaDB.getPonta(result.getLong("cabo.ponta_id_a")));
			cabo.setPontaB(PontaDB.getPonta(result.getLong("cabo.ponta_id_b")));
			// store all data into a List
			list.add(cabo);
		}

		return list;
	}

	public static Cabo getCabo(long idCabo) throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("select cabo.cabo_id, cabo_nome.nome, cabo.trecho, cabo_tipo.nome, cabo.quant_fibras, " +
						"cabo.comprimento, cabo.ponta_id_a, cabo.ponta_id_b "
						+ "from cabo, cabo_nome, cabo_tipo "
						+ "where cabo.cabo_nome_id=cabo_nome.cabo_nome_id and cabo.cabo_tipo_id=cabo_tipo.cabo_tipo_id "
						+ "and cabo.cabo_id = ?");

		ps.setLong(1, idCabo);

		// get customer data from database
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Cabo cabo = new Cabo();

			cabo.setId(result.getLong("cabo.cabo_id"));
			cabo.setNome(result.getString("cabo_nome.nome"));
			cabo.setTrecho(result.getString("cabo.trecho"));
			cabo.setTipo(result.getString("cabo_tipo.nome") + " ("
					+ result.getString("cabo.quant_fibras") + ")");
			cabo.setComprimento(result.getInt("cabo.comprimento"));
			cabo.setPontaA(PontaDB.getPonta(result.getLong("cabo.ponta_id_a")));
			cabo.setPontaB(PontaDB.getPonta(result.getLong("cabo.ponta_id_b")));

			// store all data into a List
			return cabo;
		}

		return null;
	}

	public static List<Cabo> getListCabosCon(long idPonta) throws ClassNotFoundException, SQLException {
		
		
		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("select cabo.cabo_id, cabo_nome.nome, cabo.trecho, cabo_tipo.nome, cabo.quant_fibras, " +
						"cabo.comprimento, cabo.ponta_id_a, cabo.ponta_id_b "
						+ "from cabo, cabo_nome, cabo_tipo "
						+ "where cabo.cabo_nome_id=cabo_nome.cabo_nome_id and cabo.cabo_tipo_id=cabo_tipo.cabo_tipo_id "
						+ "and ((cabo.ponta_id_a = ?) or (cabo.ponta_id_b = ?))");

		ps.setLong(1, idPonta);
		ps.setLong(2, idPonta);
			
		List<Cabo> listaCabos = new ArrayList<Cabo>();
		
		// get customer data from database
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Cabo cabo = new Cabo();

			cabo.setId(result.getLong("cabo.cabo_id"));
			cabo.setNome(result.getString("cabo_nome.nome"));
			cabo.setTrecho(result.getString("cabo.trecho"));
			cabo.setTipo(result.getString("cabo_tipo.nome") + " ("
					+ result.getString("cabo.quant_fibras") + ")");
			cabo.setComprimento(result.getInt("cabo.comprimento"));
			cabo.setPontaA(PontaDB.getPonta(result.getLong("cabo.ponta_id_a")));
			cabo.setPontaB(PontaDB.getPonta(result.getLong("cabo.ponta_id_b")));

			// store all data into a List
			listaCabos.add(cabo);
		}

		return null;
		
	}

}
