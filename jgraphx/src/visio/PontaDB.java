package visio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class PontaDB {

	// resource injection

	// @Resource(name="jdbc/mkyongdb")
	private static Connection ds;

	public static List<String> getNomePontas(int tipo) throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		List<String> temp = new ArrayList<String>();

		PreparedStatement ps = ds.prepareStatement("select nome "
				+ "from ponta " + "where ponta_tipo_id = ?");

		ps.setInt(1, tipo);

		// get customer data from database
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			// store all data into a List
			temp.add(result.getString("nome"));
		}

		return temp;
	}

	public static List<String> getNomePontas() throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		List<String> temp = new ArrayList<String>();

		PreparedStatement ps = ds.prepareStatement("select nome from ponta");

		// get customer data from database
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			// store all data into a List
			temp.add(result.getString("nome"));
		}

		return temp;
	}

	public static List<String> getFindNomePontas(String busca, int tipo)
			throws SQLException, ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		List<String> temp = new ArrayList<String>();

		PreparedStatement ps = ds.prepareStatement("select nome "
				+ "from ponta " + "where ponta_tipo_id = ? and nome like ?");

		ps.setInt(1, tipo);
		ps.setString(2, "%" + busca + "%");

		// get customer data from database
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			// store all data into a List
			temp.add(result.getString("nome"));
		}

		return temp;
	}

	public static List<String> getFindNomePontas(String busca)
			throws SQLException, ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		List<String> temp = new ArrayList<String>();

		PreparedStatement ps = ds.prepareStatement("select nome "
				+ "from ponta " + "where nome like ?");

		ps.setString(1, "%" + busca + "%");

		// get customer data from database
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			// store all data into a List
			temp.add(result.getString("nome"));
		}

		return temp;
	}

	public static Ponta getPonta(String nome) throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("select ponta_id, nome, ponta_tipo_id, endereco, cidade_id, estado_id, utm_x, utm_y "
						+ "from ponta " + "where nome=?");

		ps.setString(1, nome);

		ResultSet result = ps.executeQuery();

		while (result.next()) {

			Ponta pnt = new Ponta();

			pnt.setId(result.getLong("ponta_id"));
			pnt.setNome(result.getString("nome"));
			pnt.setTipoPonta(result.getInt("ponta_tipo_id"));
			pnt.setEndereco(result.getString("endereco"));
			pnt.setIdCidade(result.getInt("cidade_id"));
			pnt.setIdEstado(result.getInt("estado_id"));
			pnt.setUtmX(result.getInt("utm_x"));
			pnt.setUtmY(result.getInt("utm_y"));

			return pnt;

		}

		return null;
	}

	public static Ponta getPonta(long idPonta) throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("select ponta_id, nome, ponta_tipo_id, endereco, cidade_id, estado_id, utm_x, utm_y "
						+ "from ponta " + "where ponta_id=?");

		ps.setLong(1, idPonta);

		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Ponta pnt = new Ponta();

			pnt.setId(result.getLong("ponta_id"));
			pnt.setNome(result.getString("nome"));
			pnt.setTipoPonta(result.getInt("ponta_tipo_id"));
			pnt.setEndereco(result.getString("endereco"));
			pnt.setIdCidade(result.getInt("cidade_id"));
			pnt.setIdEstado(result.getInt("estado_id"));
			pnt.setUtmX(result.getInt("utm_x"));
			pnt.setUtmY(result.getInt("utm_y"));

			return pnt;
		}

		return null;
	}

	public static List<Ponta> getListPontas() throws SQLException,
			ClassNotFoundException {

		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("select ponta_id, nome, ponta_tipo_id "
						+ "from ponta");

		// get customer data from database
		ResultSet result = ps.executeQuery();

		List<Ponta> list = new ArrayList<Ponta>();

		while (result.next()) {

			Ponta pnt = new Ponta();

			pnt.setId(result.getLong("ponta_id"));
			pnt.setNome(result.getString("nome"));
			pnt.setTipoPonta(result.getInt("ponta_tipo_id"));
			pnt.setEndereco(result.getString("endereco"));
			pnt.setIdCidade(result.getInt("cidade_id"));
			pnt.setIdEstado(result.getInt("estado_id"));
			pnt.setUtmX(result.getInt("utm_x"));
			pnt.setUtmY(result.getInt("utm_y"));

			list.add(pnt);

		}

		return list;
	}

	public static void insertPonta(Ponta pontaEdit)
			throws ClassNotFoundException, SQLException {
		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("INSERT INTO ponta(nome, ponta_tipo_id, endereco, cidade_id, estado_id, utm_x, utm_y) VALUES (?,?,?,?,?,?,?) ");

		ps.setString(1, pontaEdit.getNome());
		ps.setInt(2, pontaEdit.getTipoPonta());
		ps.setString(3, pontaEdit.getEndereco());
		ps.setInt(4, pontaEdit.getIdCidade());
		ps.setInt(5, pontaEdit.getIdEstado());
		ps.setInt(6, pontaEdit.getUtmX());
		ps.setInt(7, pontaEdit.getUtmY());

		if (ps.execute())
			throw new SQLException("Insert Failed");

	}

	public static void updatePonta(Ponta pontaEdit)
			throws ClassNotFoundException, SQLException {
		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("UPDATE ponta SET nome=?, ponta_tipo_id=?, endereco=?, cidade_id=?, estado_id=?, utm_x=?, utm_y=? "
						+ "where ponta_id=?");

		ps.setString(1, pontaEdit.getNome());
		ps.setInt(2, pontaEdit.getTipoPonta());
		ps.setString(3, pontaEdit.getEndereco());
		ps.setInt(4, pontaEdit.getIdCidade());
		ps.setInt(5, pontaEdit.getIdEstado());
		ps.setInt(6, pontaEdit.getUtmX());
		ps.setInt(7, pontaEdit.getUtmY());

		ps.setLong(8, pontaEdit.getId());

		if (ps.execute())
			throw new SQLException("Update Failed");

	}

	public static void excluirPonta(Ponta pontaEdit)
			throws ClassNotFoundException, SQLException {
		if (ds == null)
			ds = MySQL.connect();

		if (ds == null)
			throw new SQLException("Can't get database connection");

		PreparedStatement ps = ds
				.prepareStatement("DELETE FROM ponta WHERE ponta_id=?");

		ps.setLong(1, pontaEdit.getId());

		if (ps.execute())
			throw new SQLException("Delete Failed");

	}

}
