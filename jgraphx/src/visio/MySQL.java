package visio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

	public static Connection connect() throws ClassNotFoundException, SQLException {
		
		String driverName = "com.mysql.jdbc.Driver";

		Class.forName(driverName);

		// Configurando a nossa conex�o com um banco de dados//

		String serverName = "localhost"; // caminho do servidor do BD

		String mydatabase = "visio"; // nome do seu banco de dados

		String url = "jdbc:mysql://" + serverName + "/" + mydatabase;

		String username = "root"; // nome de um usu�rio de seu BD

		String password = ""; // sua senha de acesso

		return DriverManager.getConnection(url, username, password);
	}
}
