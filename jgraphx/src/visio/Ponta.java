package visio;

import java.util.List;

import com.mxgraph.view.mxGraph;

public class Ponta {

	private long id;
	private String nome;
	private int tipoPonta;
	private String endereco;
	private int idCidade;
	private int idEstado;
	private int utmX;
	private int utmY;

	Ponta() {

	}

	public long getId() {
		return id;
	}

	public void setId(long l) {
		this.id = l;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getTipoPonta() {
		return tipoPonta;
	}

	public void setTipoPonta(int tipoPonta) {
		this.tipoPonta = tipoPonta;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public int getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(int idCidade) {
		this.idCidade = idCidade;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public int getUtmX() {
		return utmX;
	}

	public void setUtmX(int utmX) {
		this.utmX = utmX;
	}

	public int getUtmY() {
		return utmY;
	}

	public void setUtmY(int utmY) {
		this.utmY = utmY;
	}

	@Override
	public boolean equals(Object Obj) {

		Ponta outra = (Ponta) Obj;

		return this.getId() == outra.getId();
	}

}
